package com.tus.mase.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.data.EventDAO;
import com.tus.mase.data.UEEventIdCauseCodeDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.UEEvent;
import com.tus.mase.rest.EventWS;
import com.tus.mase.rest.UEClassWS;

public class UEClassWStest {
	
	//UEEventIdCauseCodeDAO uEEventIdCauseCodeDAO = new UEEventIdCauseCodeDAO();
	
	private UEClassWS ueClassWS = new UEClassWS();
	private Response response = mock(Response.class);
	private UEEventIdCauseCodeDAO ueEventIdCauseCodeDAO  = mock(UEEventIdCauseCodeDAO.class);
	private UEEvent euEvent = new UEEvent();
	private Query query = mock(Query.class);
	private EntityManager entityManager = mock(EntityManager.class);
	private List<Event> list;

	@BeforeEach
	public void injectMockEventDAO() throws Exception {
		ueClassWS.setuEEventIdCauseCodeDAO(ueEventIdCauseCodeDAO);
		list = new ArrayList<>();
		ueEventIdCauseCodeDAO.setEntityManager(entityManager);
		

	}
	
	@Test
	public void testGetEventById() throws SQLException {
		
		List<Object[]> expected = new ArrayList<>();
		Object [] expectedObject = new Object[10];
		expectedObject[0] = 4097;
		expectedObject[1] = "1.0";
		expectedObject[2] = "9109 PA";
		expected.add(expectedObject);
		

		when(ueClassWS.getuEEventIdCauseCodeDAO().getEventIdCauseCodebyModelNumber("9109 PA")).thenReturn(expected);
		UEEvent ueEvent = new UEEvent();
		ueEvent.setModel("9109 PA");
		ueEvent.setEventId(4097);
		ueEvent.setCauseCode("1.0");
		ueClassWS.setOutput(expected);
		
		List<UEEvent> ueEventActual = (List<UEEvent>) ueClassWS.getEventById("9109 PA").getEntity();
		assertEquals(ueEventActual.get(0).getModel(), ueEvent.getModel());
		assertEquals(ueEventActual.get(0).getEventId(), ueEvent.getEventId());
		assertEquals(ueEventActual.get(0).getCauseCode(), ueEvent.getCauseCode());
	}

	@Test
	public void testGetAllModel() throws SQLException {
		List<String> expected = new ArrayList<>();
		expected.add("G410");
		expected.add("test");
		when(ueEventIdCauseCodeDAO.getAllModels()).thenReturn(expected);
		
		assertEquals(expected, ueClassWS.getAllModels().getEntity());
		
		
	}
	

}
