package com.tus.mase.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.UserDAO;
import com.tus.mase.model.Users;
import com.tus.mase.rest.UserWS;

class UserWSTest {
	
	private UserWS userWS = new UserWS();
	private UserDAO userDAO = mock(UserDAO.class);
	private Response response = mock(Response.class);
	private List<Users> list;
	private Query query = mock(Query.class);
	
	@BeforeEach
	public void injectMockUserDAO() throws Exception {
		userWS.setUserDAO(userDAO);
		list = new ArrayList<>();
	}

	@Test
	void testUserDao() {
		userDAO = new UserDAO();
		userWS.setUserDao(userDAO);
		assertEquals(userWS.getUserDao(), userDAO);
	}
	
	@Test
	void testFindAll() {
		Users user = new Users();
		user.setUsername("test");
		list.add(user);
		when(query.getResultList()).thenReturn(list);
		when(userDAO.getAllUsers()).thenReturn(list);
		assertEquals(Response.status(200).entity(list).build().getEntity(), userWS.FindAll().getEntity());
	}
	
	@Test
	void testUpdateUser() {
		Users user = new Users();
		user.setUsername("newTest");
		userWS.updateUser(user);
		verify(userDAO, new Times(1)).update(user);
	}
	
	@Test
	public void testGetUserById() {
		Users user = new Users();
		user.setUserId(999);
		user.setUsername("test");
		when(userWS.getUserDAO().getUser("test")).thenReturn(user);
		assertEquals(userWS.getUser("test"), user);
	}
	
	@Test
	void testDeleteUser() {
		Users user = new Users();
		String name = "test";
		user.setUsername(name);
		userWS.deleteUser(name);
		verify(userDAO, new Times(1)).delete(name);
	}

	@Test
	public void testGetUserByIdNotFound() {
		when(userWS.getUserDAO().getUser("TEST")).thenReturn(null);
		assertNull(userWS.getUser("TEST"));
	}

	@Test
	public void testAddUserSuccess() {
		Users user1 = new Users();
		user1.setUserId(999);
		user1.setUsername("test");
		when(userWS.getUserDAO().getUser(user1.getUsername())).thenReturn(null);
		String failureMessage = "{\"message\":\"User " + user1.getUsername()
				+ " successfully added to the database\"}";
		assertEquals(failureMessage, userWS.addUser(user1));
	}

	@Test
	public void testAddUserFail() {
		Users user = new Users();
		user.setUserId(999);
		user.setUsername("test");
		when(userWS.getUserDAO().getUser(user.getUsername())).thenReturn(user);
		String failureMessage = "{\"message\":\"User " + user.getUsername()
				+ " has been found\"}";
		assertEquals(failureMessage, userWS.addUser(user));
	}


	@Test
	public void testUserLoginWrongPassword() {
		int id = 999;
		String password = "password";
		String incorrectPassword = "test";
		String username = "test";
		Users user = new Users();
		user.setUserId(id);
		user.setPassword(password);
		user.setUsername(username);
		response = Response.status(200).build();
		userWS.setResponse(response);
		when(userDAO.getUser("test")).thenReturn(user);
		assertEquals("fail", userWS.userLogin(username, incorrectPassword).getEntity().toString());
	}
	
	@Test
	public void testUserLoginUnknownUser() {
		int id = 999;
		String password = "password";
		String username = "test";
		Users user = new Users();
		user.setUserId(id);
		user.setPassword(password);
		user.setUsername(username);
		
		when(userWS.getUserDAO().getUser(anyObject())).thenReturn(null);
		assertEquals("fail", userWS.userLogin(username, password).getEntity().toString());
	}

}
