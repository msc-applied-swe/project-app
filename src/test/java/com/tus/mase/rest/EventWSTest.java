package com.tus.mase.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.EventDAO;
import com.tus.mase.model.Event;
import com.tus.mase.rest.EventWS;

class EventWSTest {
	
	private EventWS eventWS = new EventWS();
	private EventDAO eventDAO  = mock(EventDAO.class);
	private Event event = new Event();
	private Query query = mock(Query.class);
	private EntityManager entityManager = mock(EntityManager.class);
	private List<Event> list;

	@BeforeEach
	public void injectMockEventDAO() throws Exception {
		eventWS.setEventDAO(eventDAO);
		list = new ArrayList<>();
		

	}
	
	@Test
	void testFindAll() {
		list.add(event);
		when(query.getResultList()).thenReturn(list);
		when(eventDAO.getAllEvents()).thenReturn(list);
		assertEquals(Response.status(200).entity(list).build().getEntity(), eventWS.findAllEvent().getEntity());
	}
	
	@Test
	void testGetEventByIMSI() {
		list.add(event);
		BigInteger bigInt = new BigInteger("999999999999");
		when(query.getResultList()).thenReturn(list);
		when(eventDAO.getEventByIMSI(bigInt)).thenReturn(list);
		assertEquals(Response.status(200).entity(list).build().getEntity(), eventWS.getEventByIMSI(bigInt).getEntity());
	}
	
	
	@Test
	public void testGetEventById() {
		Event event = new Event();
		event.setEventId(999);

		when(eventWS.getEventDAO().getEvent(999)).thenReturn(event);
		assertEquals(eventWS.getEventById(999).getEntity(), event);
	}
	
	@Test
	public void testGetEventByIdNotFound() {
		when(eventWS.getEventDAO().getEvent(997)).thenReturn(null);
		assertNull(eventWS.getEventById(997).getEntity());
	}
	
	@Test
	void testSaveEvent() {
		assertEquals(eventWS.saveEvent(event).getEntity(), event);
		verify(eventDAO, new Times(1)).save(event);
	}
	
	@Test
	void testgetEventByDateAndPhone() throws ParseException {
		String date2 = "2022-01-01 00:00";
		String date3 = "2022-01-01 01:00";
		int val = 1;
		list.add(event);
		when(query.getResultList()).thenReturn(list);
		when(eventDAO.getEventByDateAndPhone(anyObject(), anyObject(), anyInt())).thenReturn(list);
		assertEquals(eventWS.getEventFailureByDateAndPhone(date2, date3, val).getEntity().toString(), list.toString());
	}
	
	@Test
	void testGetEventFailureByDateAndIMSI() throws ParseException {
		list.add(event);
		String date1 = "2022-01-01 00:00";
		String date2 = "2022-01-01 01:00";
		when(eventDAO.getEventByDate(anyObject(), anyObject())).thenReturn(list);
		assertEquals(eventWS.getEventFailureByDate(date1, date2).getEntity().toString(), list.toString());
	}
	
	@Test
	void testGetEventFailureByDateAndIMSIWithParseException() {
		list.add(event);
		String date1 = "202222222-01-0111 9 f0d d000:00";
		String date2 = "2022-01-01 01:00";
		when(eventDAO.getEventByDate(anyObject(), anyObject())).thenReturn(list);
		Throwable exception = assertThrows(ParseException.class, () ->{
			eventWS.getEventFailureByDate(date1, date2);
		});
		assertEquals("Unparseable date: \""+ date1 + "\"", exception.getMessage());
	}
	
	@Test
	void testgetEventFailureByDateAndIMSI() throws ParseException {
		String date2 = "2022-01-01 00:00";
		String date3 = "2022-01-01 01:00";
		BigInteger bigInt = new BigInteger("999999999999");
		list.add(event);
		when(eventDAO.getEventByDateAndIMSI(anyObject(), anyObject(), anyObject())).thenReturn(list);
		assertEquals(eventWS.getEventFailureByDateAndIMSI(date2, date3, bigInt).getEntity().toString(), list.toString());
	}
	
	@Test
	void testgetCauseCodebyIMSI()throws ParseException{
		final BigInteger IMSI = new BigInteger("191911000265168");
		List<String> causeCodeList = Arrays.asList("2","8");
		when(eventDAO.getcauseCodebyIMSI(IMSI)).thenReturn(causeCodeList);
		assertEquals(eventWS.getCauseCodebyIMSI(IMSI).getEntity(), causeCodeList);
	}
	
	@Test
	void testgetAllIMSI()throws ParseException{
		
		List<String> ListofIMSI = Arrays.asList("191911000516761");
		when(eventDAO.getAllIMSI()).thenReturn(ListofIMSI);
		assertEquals(ListofIMSI, eventWS.findAllIMSI().getEntity());
	}

	@Test
	void testGetTopTenCombinations() throws ParseException {
		String date2 = "2022-01-01 00:00";
		String date3 = "2022-01-01 01:00";
		list.add(new Event());
		when(eventDAO.getTopTenCombinations(anyObject(), anyObject())).thenReturn(list);
		assertEquals(eventWS.getTopTenCombinations(date2, date3).getEntity().toString(), list.toString());
	}

}
