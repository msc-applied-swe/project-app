package com.tus.mase.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.EventCauseDAO;
import com.tus.mase.model.EventCause;
import com.tus.mase.rest.EventCauseWS;

class EventCauseWSTest {
	
	private EventCauseWS eventCauseWS = new EventCauseWS();
	private EventCauseDAO eventCauseDAO = mock(EventCauseDAO.class);
	private EventCause eventCause = new EventCause();
	private Query query = mock(Query.class);
	private EntityManager entityManager = mock(EntityManager.class);
	private List<EventCause> list;

	@BeforeEach
	public void injectMockEventCauseDAO() throws Exception {
		eventCauseWS.setEventCauseDAO(eventCauseDAO);
		list = new ArrayList<>();
		
	}
	
	@Test
	void testFindAll() {
		EventCause eventCause = new EventCause();
		eventCause.setEventId(22);
		list.add(eventCause);
		when(query.getResultList()).thenReturn(list);
		when(eventCauseDAO.getAllEventCauses()).thenReturn(list);
		assertEquals(Response.status(200).entity(list).build().getEntity(), eventCauseWS.findAllEventCauses().getEntity());
	}
	
	@Test
	public void testGetEventCauseById() {
		EventCause eventCause = new EventCause();
		eventCause.setEventId(999);

		when(eventCauseWS.getEventCauseDAO().getEventCause(999)).thenReturn(eventCause);
		assertEquals(eventCauseWS.getEventCausesById(999).getEntity(), eventCause);
	}
	
	@Test
	public void testGetEventCauseByIdNotFound() {
		when(eventCauseWS.getEventCauseDAO().getEventCause(997)).thenReturn(null);
		assertNull(eventCauseWS.getEventCausesById(997).getEntity());
	}
	
	@Test
	void testSaveEventCause() {
		assertEquals(eventCauseWS.saveEventCauses(eventCause).getEntity(), eventCause);
		verify(eventCauseDAO, new Times(1)).save(eventCause);
	}
	

}
