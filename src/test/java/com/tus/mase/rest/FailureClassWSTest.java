package com.tus.mase.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.ws.rs.core.Response;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.model.FailureClass;

public class FailureClassWSTest {

	private FailureClass failureClass;
	private FailureClassWS failureClassWS;
	private FailureClassDAO failureClassDAO;
	private Query query = mock(Query.class);
	@SuppressWarnings("unused")
	private EntityManager entityManager;
	private List<FailureClass> list;

	@BeforeEach
	void setUpFailureClassDAO() throws Exception {
		failureClass = new FailureClass();
		failureClassWS = new FailureClassWS();
		failureClassDAO = mock(FailureClassDAO.class);
		failureClassWS.setFailureClassDAO(failureClassDAO);
		entityManager = mock(EntityManager.class);
		list = new ArrayList<>();
	}

	@Test
	void testFindAllFailureClass() {
		list.add(failureClass);
		when(query.getResultList()).thenReturn(list);
		when(failureClassDAO.getAllFailureClasses()).thenReturn(list);
		assertEquals(Response.status(200).entity(list).build().getEntity(),
				failureClassWS.findAllFailureClasses().getEntity());
	}

	@Test
	void testGetFailureClassById() {
		failureClass.setDescription("MO DATA");
		when(failureClassWS.getFailureClassDAO().getFailureClass(1)).thenReturn(failureClass);
		assertEquals("MO DATA", failureClass.getDescription());
		assertEquals(((FailureClass) failureClassWS.getFailureClassById(1).getEntity()).getDescription(),
				failureClass.getDescription());
	}

	@Test
	void testSaveFailureClass() {
		assertEquals(failureClassWS.saveFailureClass(failureClass).getEntity(), failureClass);
		verify(failureClassDAO, new Times(1)).save(failureClass);
	}

}
