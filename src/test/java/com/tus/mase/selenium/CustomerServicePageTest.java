package com.tus.mase.selenium;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

class CustomerServicePageTest {

	WebDriver driver;
	long start;
	String cust_serv_ID="Customer Service";
	String cust_serv_pass="password";
	String IMSI="191911000456426";
	URL url;
	long finish;
	long totalTime;

	@BeforeEach
	void setUp() throws MalformedURLException {
		driver=null;
		WebDriverManager.chromedriver().version("99.0.4844.51").setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		start = System.currentTimeMillis();
		driver.get("http://localhost:8080/project-app/");
		url = new URL("http://localhost:8080/project-app/");

	}

	@AfterEach
	void teardown() {
		driver.quit();
	}
	@RepeatedTest(3)
	public void testloginCustRepSuccess() throws IOException {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(cust_serv_ID);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(cust_serv_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		start = System.currentTimeMillis();
		login.click();
		assertEquals(200, connection.getResponseCode());
		finish = System.currentTimeMillis();
		totalTime = finish - start; 
		System.out.println("Total Time for Login for customer service represetative - "+totalTime); 
		assertTrue(totalTime<2000);
		
	}
	@RepeatedTest(3)
	public void testQueryIMSI() throws IOException {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(cust_serv_ID);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(cust_serv_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		
		WebElement ImsiButton=driver.findElement(By.xpath("//*[@id=\"customerServiceCard\"]/div[2]/a"));
		ImsiButton.click();
		WebElement serachButton=driver.findElement(By.xpath("//*[@id=\"imsiNumber\"]"));
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		start = System.currentTimeMillis();
		serachButton.sendKeys(IMSI);
		WebElement faiulrebutton=driver.findElement(By.xpath("//*[@id=\"submitButton\"]"));
		faiulrebutton.click();
		assertEquals(200, connection.getResponseCode());
		finish = System.currentTimeMillis();
		totalTime = finish - start; 
		System.out.println("Total Time for Login for customer service represetative - "+totalTime); 
		assertTrue(totalTime<2000);
		
	}

}
