package com.tus.mase.selenium;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class NetworkEngineerPageTest {

	WebDriver driver;
	long start;
	String netWorkEngID="Network Engineer";
	String netEngpass="password";
	String IMSI="191911000456426";
	URL url;
	long finish;
	long totalTime;

	@BeforeEach
	void setUp() throws MalformedURLException {
		driver=null;
		WebDriverManager.chromedriver().version("99.0.4844.51").setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		start = System.currentTimeMillis();
		driver.get("http://localhost:8080/project-app/");
		url = new URL("http://localhost:8080/project-app/");

	}
	
	@AfterEach
	void teardown() {
		driver.quit();
	}
	@RepeatedTest(3)
	public void testloginNetEngTime() throws IOException {
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(netWorkEngID);		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(netEngpass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		start = System.currentTimeMillis();
		login.click();
		assertEquals(200, connection.getResponseCode());
		finish = System.currentTimeMillis();
		totalTime = finish - start; 
		System.out.println("Total Time for Login for admin - "+totalTime); 
		assertTrue(totalTime<2000);
	}
	@RepeatedTest(3)
	public void queryIMSITime() throws IOException {
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(netWorkEngID);		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(netEngpass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		WebElement failures=driver.findElement(By.xpath("//*[@id=\"customerServiceCard\"]/div[2]/a"));
		HttpURLConnection connection = (HttpURLConnection)url.openConnection();
		start = System.currentTimeMillis();
		failures.click();
		WebElement IMSIbox=driver.findElement(By.xpath("//*[@id=\"imsiNumber\"]"));
		IMSIbox.sendKeys(IMSI);
		assertEquals(200, connection.getResponseCode());
		finish = System.currentTimeMillis();
		totalTime = finish - start; 
		System.out.println("Total Time for IMSI search - "+totalTime); 
		assertTrue(totalTime<20000);
	}

	
}
