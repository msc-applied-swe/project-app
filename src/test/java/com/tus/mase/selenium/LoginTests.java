package com.tus.mase.selenium;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

class LoginTests {
	WebDriver driver;
	String adminID="Administrator";
	String adminpass="password";
	String cust_serv_ID="Customer Service";
	String cust_serv_pass="password";
	String Net_Eng_ID="Network Engineer";
	String Net_Eng_pass="password";
	String support_engId="Support Engineer";
	String support_eng_pass="password";
	String net_manEng_Id="Ashling";
	String net_manEng_pass="Keening";
	
	@BeforeEach
	void setUp() {
		//System.setProperty("webdriver.chrome.driver", "C:\\Data\\chromedriver.exe");
		//driver=new ChromeDriver();
		driver=null;
		WebDriverManager.chromedriver().version("99.0.4844.51").setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		driver.get("http://localhost:8080/project-app/");
	}
	
	@AfterEach
	void teardown() {
		driver.quit();
	}
	
	@Test
	public void testTitle() {
		assertEquals("Login - - ENM MASE 2022", driver.getTitle());
	}
	
	@Test
	public void testloginAdminSuccess() {
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(adminID);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(adminpass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		String adminpage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/project-app/dashboard.html", adminpage);
		
	}
	@Test
	public void testloginCustRepSuccess() {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(cust_serv_ID);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(cust_serv_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		String cut_ser_page=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/project-app/dashboard.html", cut_ser_page);
		
	}
	@Test
	public void testloginNetEngSuccess() {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(Net_Eng_ID);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(Net_Eng_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		String net_eng_page=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/project-app/dashboard.html", net_eng_page);
		
	}
	
	@Test
	public void testlogiSuppEngSuccess() {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(support_engId);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(support_eng_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		String net_eng_page=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/project-app/dashboard.html", net_eng_page);
		
	}
	
	@Test
	public void testlogiNetManEngSuccess() {
		
		WebElement userId=driver.findElement(By.id("inputEmail"));
		userId.sendKeys(net_manEng_Id);
		
		WebElement pass=driver.findElement(By.id("inputPassword"));
		pass.sendKeys(net_manEng_pass);
		WebElement login=driver.findElement(By.xpath("//*[@id=\"loginRedirect\"]"));
		login.click();
		String net_eng_page=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/project-app/dashboard.html", net_eng_page);
		
	}
	
}
