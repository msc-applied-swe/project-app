package com.tus.mase.test.unit;

import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.FailureClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FailureClassDAOTest {

    private EntityManager em;
    private Query query;
    private List<Event> eventList;
    private List<FailureClass> failureClassList;
    private Event event;
    private FailureClass failureClass;
    private FailureClassDAO failureClassDAO;

    @BeforeEach
    public void setup() {
        eventList = new ArrayList<>();
        failureClassList = new ArrayList<>();
        em = mock(EntityManager.class);
        query = mock(Query.class);
        event = new Event();
        event.setEventId(1);
        failureClass = new FailureClass();
        failureClass.setFailureClass(1);
        eventList.add(event);
        failureClassList.add(failureClass);
        failureClassDAO = new FailureClassDAO();
        failureClassDAO.setEntityManager(em);

    }

    @Test
    void testGetAllFailures() {
        when(em.createQuery("SELECT f FROM FailureClass f")).thenReturn(query);
        query = em.createQuery("SELECT f FROM FailureClass f");
        when(query.getResultList()).thenReturn(failureClassList);
        assertEquals(failureClassDAO.getAllFailureClasses().toString(), failureClassList.toString());
    }

    @Test
    void testGetFailureById() {
        when(em.createQuery("SELECT DISTINCT(e.IMSI), e.failureClass from Event e WHERE e.failureClass=:id")).thenReturn(query);
        query = em.createQuery("SELECT DISTINCT(e.IMSI), e.failureClass from Event e WHERE e.failureClass=:id");
        query.setParameter("id", "1.0");
        when(query.getResultList()).thenReturn(eventList);
        assertEquals(failureClassDAO.getFailureById(1).toString(), eventList.toString());
    }
}
