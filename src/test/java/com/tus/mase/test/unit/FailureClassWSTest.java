package com.tus.mase.test.unit;

import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.FailureClass;
import com.tus.mase.rest.FailureClassWS;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.Query;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FailureClassWSTest {

    private FailureClassDAO failureClassDAO = mock(FailureClassDAO.class);
    private FailureClassWS failureClassWS = new FailureClassWS();
    private FailureClass failureClass;
    private Event event;
    private List<FailureClass> failureClassList;
    private List<Event> eventList;
    private Query query;


    @BeforeEach
    public void setup() {
        failureClassWS.setFailureClassDAO(failureClassDAO);
        query = mock(Query.class);
        failureClass = new FailureClass();
        event = new Event();
        failureClassList = new ArrayList<>();
        eventList = new ArrayList<>();
        failureClassList.add(failureClass);
        eventList.add(event);
    }

    @Test
    void testFindAllFailures() {
        when(query.getResultList()).thenReturn(failureClassList);
        when(failureClassDAO.getAllFailureClasses()).thenReturn(failureClassList);
        assertEquals(failureClassWS.findAllFailures().getEntity(), Response.status(200).entity(failureClassList).build().getEntity());
    }

    @Test
    void testGetEventByIMSI() {
        when(query.getResultList()).thenReturn(eventList);
        when(failureClassDAO.getFailureById(anyInt())).thenReturn(eventList);
        assertEquals(failureClassWS.getEventByIMSI(1).getEntity(), Response.status(200).entity(eventList).build().getEntity());
    }

}
