package com.tus.mase.data;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

class DatabaseHelperTest {
	private DatabaseHelper databaseHelper;
	private static final String NATIVE_QUERY = "CALL truncate_tables()";
	private EntityManager em;
	private Query query;

	@BeforeEach
	void setUp() {
		em = mock(EntityManager.class);
		query = mock(Query.class);
		databaseHelper = new DatabaseHelper();
		databaseHelper.setEntityManager(em);
		databaseHelper.setQuery(query);
		when(em.createNativeQuery(NATIVE_QUERY)).thenReturn(query);
	}

	@Test
	void callTruncateTablesTest() {
		databaseHelper.truncateTables();
		verify(em, new Times(1)).createNativeQuery(NATIVE_QUERY);
		verify(query, new Times(1)).executeUpdate();
	}
}
