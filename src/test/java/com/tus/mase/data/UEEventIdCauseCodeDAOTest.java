package com.tus.mase.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.tus.mase.data.UEEventIdCauseCodeDAO;
import com.tus.mase.model.UEEvent;

public class UEEventIdCauseCodeDAOTest {

	private UEEventIdCauseCodeDAO uEEventIdCauseCodeDAO = new UEEventIdCauseCodeDAO();
	private EntityManager em;
	private static final String MODEL = "G410";
	private Query query;
	List<Object[]> expected;
	private List<UEEvent> actual;
	UEEvent ueEvent;

	@BeforeEach
	public void setup() {
		em = mock(EntityManager.class);
		uEEventIdCauseCodeDAO = new UEEventIdCauseCodeDAO();
		uEEventIdCauseCodeDAO.setEntityManager(em);
		query =  mock(Query.class);
		ueEvent = new UEEvent();
		expected = new ArrayList<>();
	}
	
	@Test
	public void testgetEventIdCauseCodebyModelNumber() throws SQLException {
		
		when(em.createNativeQuery(anyString())).thenReturn(query);
		uEEventIdCauseCodeDAO.setQuery(query);
		expected.add(new Object[10]);
		when(query.getResultList()).thenReturn(expected);
		assertNotNull(uEEventIdCauseCodeDAO.getQuery());
		assertNotNull(uEEventIdCauseCodeDAO.getEntityManager());
		assertEquals(uEEventIdCauseCodeDAO.getEventIdCauseCodebyModelNumber("9109 PA"), expected);
		
	}
	
	
	@Test
	public void testgetAllModels() throws SQLException {
		
		when(em.createNativeQuery(anyString())).thenReturn(query);
		uEEventIdCauseCodeDAO.setQuery(query);
		expected.add(new Object[10]);
		when(query.getResultList()).thenReturn(expected);
		assertNotNull(uEEventIdCauseCodeDAO.getQuery());
		assertNotNull(uEEventIdCauseCodeDAO.getEntityManager());
		assertEquals(uEEventIdCauseCodeDAO.getAllModels(), expected);
		
	}
	
}
