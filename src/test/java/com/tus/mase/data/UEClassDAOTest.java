package com.tus.mase.data;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.model.UEClass;

class UEClassDAOTest {

	private UEClass ueClass;
	private EntityManager em;
	private UEClassDAO ueClassDAO;
	private Query query;
	private List<UEClass> expected, actual;

	@BeforeEach
	void setUp() throws Exception {
		ueClass = new UEClass();
		em = mock(EntityManager.class);
		ueClassDAO = new UEClassDAO();
		ueClassDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT u FROM UEClass u")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = ueClassDAO.getAllUEClasses();
	}

	@Test
	void testGetAllUEClasses() {
		assertSame(expected, actual);
	}

	@Test
	public void testGetUEClass() {
		ueClass.setTAC(33001235);
		when(ueClassDAO.getEntityManager().find(UEClass.class, ueClass.getTAC()))
				.thenReturn(ueClass);
		assertEquals(ueClassDAO.getUEClass(33001235), ueClass);
	}

	@Test
	void testSaveUEClass() {
		ueClassDAO.save(ueClass);
		verify(em, new Times(1)).persist(ueClass);
	}
}
