package com.tus.mase.data;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.model.MCCMNC;
import com.tus.mase.model.MCCMNCid;

class MCCMNCDAOTest {

	private MCCMNC mccmnc;
	private EntityManager em;
	private MCCMNCDAO mccMncDAO;
	private Query query;
	private List<MCCMNC> expected, actual;
	private int mcc;
	private int mnc;
	private MCCMNCid mCCMNCid1;
	
	@BeforeEach
	void setUp() throws Exception {
		mccmnc = new MCCMNC();
		em = mock(EntityManager.class);
		mccMncDAO = new MCCMNCDAO();
		mccMncDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT m FROM MCCMNC m")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = mccMncDAO.getAllMCCMNC();
		mCCMNCid1 = new MCCMNCid(mcc, mnc);
	}

	@Test
	void testGetAllMCCMNCClasses() {
		assertSame(expected, actual);
	}

	@Test
	void testGetMCCMNCClass() {
		mccmnc.setCountry("Guadeloupe-France");
		when(mccMncDAO.getEntityManager().find(MCCMNC.class, mCCMNCid1))
				.thenReturn(mccmnc);
		assertEquals(mccMncDAO.getMCCMNC(mCCMNCid1), mccmnc);
	}

	@Test
	void testSaveMCCMNC() {
		mccMncDAO.save(mccmnc);
		verify(em, new Times(1)).persist(mccmnc);
	}

}
