package com.tus.mase.data;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.model.EventCause;
import com.tus.mase.model.FailureClass;

class FailureClassDAOTest {

	private FailureClass failureClass;
	private EntityManager em;
	private FailureClassDAO failureClassDAO;
	private Query query;
	private List<FailureClass> expected, actual;

	@BeforeEach
	void setUp() throws Exception {
		failureClass = new FailureClass();
		em = mock(EntityManager.class);
		failureClassDAO = new FailureClassDAO();
		failureClassDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT f FROM FailureClass f")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = failureClassDAO.getAllFailureClasses();
	}

	@Test
	public void testGetAllFailureClasses() {
		assertSame(expected, actual);
	}

	@Test
	public void testGetFailureClass() {
		failureClass.setFailureClass(1);
		when(failureClassDAO.getEntityManager().find(FailureClass.class, failureClass.getFailureClass()))
				.thenReturn(failureClass);
		assertEquals(failureClassDAO.getFailureClass(1), failureClass);
	}

	@Test
	void testSaveFailureClass() {
		failureClassDAO.save(failureClass);
		verify(em, new Times(1)).persist(failureClass);
	}
}
