package com.tus.mase.data;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.EventCauseDAO;
import com.tus.mase.model.EventCause;
import com.tus.mase.model.Users;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class EventCauseDAOTest {
	
	private EventCause eventCause;
	private EntityManager em;
	private EventCauseDAO eventCauseDAO;
	private Query query;
	private List<EventCause> expected, actual;

	@BeforeEach
	public void setup() {
		em = mock(EntityManager.class);
		eventCauseDAO = new EventCauseDAO();
		eventCauseDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT u FROM EventCause u")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = eventCauseDAO.getAllEventCauses();
	}

	@Test
	public void testGetAllEventCauses() {
		assertSame(expected, actual);
	}
	
	@Test
	public void testGetEventCause() {
		EventCause eventCause = new EventCause();
		eventCause.setCauseCode(22);
		when(eventCauseDAO.getEntityManager().find(EventCause.class, eventCause.getCauseCode())).thenReturn(eventCause);
		assertEquals(eventCauseDAO.getEventCause(22), eventCause);
	}
	
	@Test
	void testSaveEventCause() {
		eventCauseDAO.save(eventCause);
		verify(em, new Times(1)).persist(eventCause);
	}
	
}
