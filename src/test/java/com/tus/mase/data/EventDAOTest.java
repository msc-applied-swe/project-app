package com.tus.mase.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.model.Event;

class EventDAOTest {
	private Event event;
	private EntityManager em;
	private EventDAO eventDAO = new EventDAO();
	private Query query;
	private List<Event> expected, actual;

	@BeforeEach
	public void setup() {
		em = mock(EntityManager.class);
		event = new Event();
		eventDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT u FROM Event u")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = eventDAO.getAllEvents();
	}

	@Test
	void testGetAllEvents() {
		assertSame(expected, actual);
	}

	@Test
	void testSaveEvent() {
		eventDAO.save(event);
		verify(em, new Times(1)).persist(event);
	}

	@Test
	void testEventUpdate() {
		eventDAO.update(event);
		verify(em, new Times(1)).merge(event);
	}

	@Test
	void testEventDelete() {
		int id = event.getEventId();
		eventDAO.delete(id);
		verify(em, new Times(1)).remove(eventDAO.getEvent(id));
	}
	
	@Test
	void testgetEventByDate() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		Date date = new Date();
		Date date1 = new Date();
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getEventByDate(date, date1), expected);
	}
	
	@Test
	void testgetEventByIMSI() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		BigInteger bigInt = new BigInteger("99999999999");
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getEventByIMSI(bigInt), expected);
	}
	
	@Test
	void testgetEventByDateAndPhone() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		Date date = new Date();
		Date date1 = new Date();
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getEventByDateAndPhone(date, date1,1), expected);
	}
	
	@Test
	void testGetEventByDateAndIMSI() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		Date date = new Date();
		Date date1 = new Date();
		BigInteger bigInt = new BigInteger("99999999999");
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getEventByDateAndIMSI(date, date1, bigInt), expected);
	}

	@Test
	void testGetTopTenIMSI() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		Date date = new Date();
		Date date1 = new Date();
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getTopTenIMSI(date, date1), expected);
	}
	
	@Test
	void getcauseCodebyIMSI() {
		List<String> causeCode = Arrays.asList("5","5");
		BigInteger IMSI = new BigInteger("63473647");
		BigInteger bigInt = new BigInteger("99999999999");
		when(query.setParameter("IMSI", IMSI)).thenReturn(query);
		when(em.createQuery(anyString())).thenReturn(query);
		when(query.getResultList()).thenReturn(causeCode);
		
		assertEquals(causeCode, eventDAO.getcauseCodebyIMSI(bigInt));
	}
	
	@Test
	void  testGetAllIMSI(){
		
		List<BigInteger> ListofIMSI = Arrays.asList(new BigInteger("34234234234"));
		when(em.createQuery(anyString())).thenReturn(query);
		when(query.getResultList()).thenReturn(ListofIMSI);
		assertEquals(ListofIMSI, eventDAO.getAllIMSI());
		
	}

	@Test
	void testGetTopTenCombinations() {
		when(em.createQuery(anyString())).thenReturn(query);
		eventDAO.setQuery(query);
		Date date = new Date();
		Date date1 = new Date();
		expected.add(event);
		when(query.getResultList()).thenReturn(expected);
		assertEquals(eventDAO.getTopTenCombinations(date, date1), expected);
	}


}