package com.tus.mase.data;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.UserDAO;
import com.tus.mase.model.Users;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

public class UserDAOTest {
	private EntityManager em;
	private UserDAO userDAO;
	private Query query;
	private List<Users> expected, actual;

	@BeforeEach
	public void setup() {
		em = mock(EntityManager.class);
		userDAO = new UserDAO();
		userDAO.setEntityManager(em);
		query = mock(Query.class);
		when(em.createQuery("SELECT u FROM Users u")).thenReturn(query);
		expected = new ArrayList<>();
		when(query.getResultList()).thenReturn(expected);
		actual = userDAO.getAllUsers();
	}

	@Test
	public void testGetAllUsers() {
		List<Users> list = new ArrayList<>();
		Users user = new Users();
		user.setUsername("test");
		list.add(user);
		query = em.createQuery("SELECT u FROM Users u");
		when(query.getResultList()).thenReturn(list);
		assertEquals(userDAO.getAllUsers(), list);
	}
	
	@Test
	public void testAddUser() {
		Users user = new Users();
		user.setUsername("test");
		user.setUserId(999);
		userDAO.addUser(user);

		ArgumentCaptor<Users> addedUser = ArgumentCaptor.forClass(Users.class);
		verify(userDAO.getEntityManager(), atLeastOnce()).persist(
				addedUser.capture());

		int id = addedUser.getValue().getUserId();
		assertEquals(999, id);
		when(em.find(anyObject(), anyString())).thenReturn(user);
		userDAO.addUser(user);
		verify(em, new Times(1)).merge(user);
	}

	@Test
	public void testGetUser() {
		Users user = new Users();
		user.setUserId(22);
		user.setUsername("test");
		when(em.find(anyObject(), anyString())).thenReturn(user);
		assertEquals(userDAO.getUser(user.getUsername()), user);
	}
	
	@Test
	public void testGetUserNotFound() {
		Users user = new Users();
		user.setUserId(999);
		user.setUsername("test");
		when(userDAO.getEntityManager().find(Users.class, user.getUserId()))
				.thenReturn(null);
		assertNull(userDAO.getUser("test"));
	}
	
	@Test
	void testUserUpdate() {
		Users user = new Users();
		userDAO.update(user);
		verify(em, new Times(1)).merge(user);
	}
	
	@Test
	void testUserDelete() {
		Users user = new Users();
		user.setUsername("test");
		int id = user.getUserId();
		when(em.find(anyObject(), anyString())).thenReturn(user);
		userDAO.delete(user.getUsername());
		verify(em, new Times(1)).remove(userDAO.getUser(user.getUsername()));
	}
}
