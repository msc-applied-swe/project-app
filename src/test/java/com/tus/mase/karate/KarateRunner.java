package com.tus.mase.karate;


import com.intuit.karate.junit5.Karate;
 

public class KarateRunner {
	
	@Karate.Test
	Karate testLogin() {
		return Karate.run("user.feature").relativeTo(getClass());
	}
	@Karate.Test
	Karate testEventWebService() {
		return Karate.run("event.feature").relativeTo(getClass());
	}
 
}


