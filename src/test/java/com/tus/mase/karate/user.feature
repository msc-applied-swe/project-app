#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template



Feature: Login User 
	Background:
		* url 'http://localhost:8080/project-app/rest/'
		
	Scenario: testing the Get ALL User 
	Given path 'users' 
	When method GET
	Then status 200
	And match $ == [{"username":"Administrator","userId":1, "password":"password","type":"system administrator"}, {"username":"Ashling","userId":124,"password":"Keening","type":"network management engineer"}, {"username":"Customer Service","userId":1001,"password":"password","type":"customer service rep"}, {"username":"Network Engineer","userId":1004,"password":"password","type":"network management engineer"}, {"username":"Support Engineer","userId":1002,"password":"password","type":"support engineer"}]

 Scenario: testing the Get User by username
	Given path 'users/Ashling' 
	When method GET
	Then status 200
	And match $ == {"username":"Ashling","userId":124,"password":"Keening","type":"network management engineer"}

	Scenario: testing user login with login success
	Given path 'users/login/Ashling&Keening' 
	When method GET
	Then status 200
	And match $ == {"username":"Ashling","userId":124,"password":"Keening","type":"network management engineer"}
	
	Scenario: testing user login with login failure
	Given path 'users/login/Amy02&pass1234' 
	When method GET
	Then status 204
	
	
