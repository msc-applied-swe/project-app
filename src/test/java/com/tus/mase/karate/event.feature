
Feature: Event WebService
Background:
		* url 'http://localhost:8080/project-app/rest/'

  Scenario: FindAllEvents  
  Given path 'Event' 
	When method GET
	Then status 200





  Scenario: Find Event by id
  Given path 'Event/1' 
	When method GET
	Then status 200
	And match $ == {"date":1582318860000,"hier32_ID":4462714857032800000,"eventId":4098,"uetype":33001735,"imsi":191911000456426,"cellId":1,"neVersion":"13A","operator":1,"market":405,"duration":1005,"hier321_ID":1650127677358040000,"hier3_ID":5101480358281000000,"failureClass":"4.0","causeCode":"1.0"}
	
	Scenario: Search Event by IMSI
  Given path 'Event/search/191911000516761' 
	When method GET
	Then status 200
	
	