package com.tus.mase.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

import java.io.IOException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.repository.StartFolderWatcher.States;

class StartFolderWatcherTest {
	
	private StartFolderWatcher sfw; 
	private FolderWatcher fw;

	@BeforeEach
	void setUp(){
		sfw = new StartFolderWatcher();
		fw = mock(FolderWatcher.class);
	}

	@Test
	void setStatetest() {
		sfw.setState(States.BEFORESTARTED);
		assertEquals(States.BEFORESTARTED, sfw.getState());
	}

	@Test
	void initTest() throws IOException, InterruptedException {
		sfw.setfolderWatcher(fw);
		sfw.init();
		assertEquals(States.STARTED, sfw.getState());

	}
	
	@Test
	void destroyTest() {
		sfw.setfolderWatcher(fw);
		sfw.destroy();
		assertEquals(States.SHUTTINGDOWN, sfw.getState());
	}
}
