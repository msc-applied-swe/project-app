package com.tus.mase.repository;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.tus.mase.model.Event;
import com.tus.mase.model.EventCause;
import com.tus.mase.model.FailureClass;
import com.tus.mase.model.MCCMNC;
import com.tus.mase.model.UEClass;

class EntityFactoryTest {

	private int market = 344;
	private int operator = 930;
	private EntityFactory ef;
	private String description = "RRC CONN SETUP-SUCCESS";
	private int FfailureClass = 20;
	private EntityManager em;
	private FailureClass fclass;
	private int TAC = 100100;
	private String m_name = "G410";
	private String manufacturer = "Mitsubishi";
	private String AccessCapability = "GSM 1800, GSM 900";
	private UEClass ueobj;
	private MCCMNC mnc_mcc;
	private String country = "Denark";
	private String mcc_mncope = "TDC-Dk";
	private String Model = "RAP40GW";
	private String vendorName = "RIM";
	private String Ue_Type = "HANDHELD";
	private String Os = "BLACKBERRY";
	private String inputMode = "QWERTY";
	private String filename = "src/test/resources/validTestData.xls";
	private HSSFWorkbook workBook;
	private HSSFSheet workBookSheet;
	private HSSFCell[] cellData;

	@BeforeEach
	public void setUp() {
		ef = new EntityFactory();
		em = mock(EntityManager.class);
		fclass = new FailureClass(FfailureClass, description);
		ueobj = new UEClass(TAC, m_name, manufacturer, AccessCapability, Model, vendorName, Ue_Type, Os, inputMode);
		mnc_mcc = new MCCMNC(market, operator, country, mcc_mncope);
	}

	@Test
	void creatEventTest() throws IOException {
		Map<Integer, List<HSSFCell>> data = getWorkBookData(filename, 0);
		cellData = new HSSFCell[14];
		for (Map.Entry<Integer, List<HSSFCell>> entry : data.entrySet()) {
			for (int i = 0; i < cellData.length; i++) {
				cellData[i] = entry.getValue().get(i);
			}
		}
		Event event = ef.createEvent(cellData);
		assertEquals(event.getClass(), Event.class);
		assertEquals("1.0", event.getFailureClass());
	}

	@Test
	void createEventCauseTest() throws IOException {
		Map<Integer, List<HSSFCell>> data = getWorkBookData(filename, 1);
		cellData = new HSSFCell[3];
		for (Map.Entry<Integer, List<HSSFCell>> entry : data.entrySet()) {
			for (int i = 0; i < cellData.length; i++) {
				cellData[i] = entry.getValue().get(i);
			}
		}
		EventCause eventCause = ef.createEventCause(cellData);
		assertEquals(eventCause.getClass(), EventCause.class);
		assertEquals(14, eventCause.getCauseCode());
	}

	@Test
	void createFailureClassTest() throws IOException {
		Map<Integer, List<HSSFCell>> data = getWorkBookData(filename, 2);
		cellData = new HSSFCell[2];
		for (Map.Entry<Integer, List<HSSFCell>> entry : data.entrySet()) {
			for (int i = 0; i < cellData.length; i++) {
				cellData[i] = entry.getValue().get(i);
			}
		}
		FailureClass failureClass = ef.createFailureClass(cellData);
		assertEquals(failureClass.getClass(), FailureClass.class);
		assertEquals("MO DATA", failureClass.getDescription());
		;
	}

	@Test
	void createUETest() throws IOException {
		Map<Integer, List<HSSFCell>> data = getWorkBookData(filename, 3);
		cellData = new HSSFCell[9];
		for (Map.Entry<Integer, List<HSSFCell>> entry : data.entrySet()) {
			for (int i = 0; i < cellData.length; i++) {
				cellData[i] = entry.getValue().get(i);
			}
		}
		UEClass UEClass = ef.createUE(cellData);
		assertEquals(UEClass.getClass(), UEClass.class);
		assertEquals("Test IMEI", UEClass.getMarketing());
	}

	@Test
	void createMCCMNCTest() throws IOException {
		Map<Integer, List<HSSFCell>> data = getWorkBookData(filename, 4);
		cellData = new HSSFCell[4];
		for (Map.Entry<Integer, List<HSSFCell>> entry : data.entrySet()) {
			for (int i = 0; i < cellData.length; i++) {
				cellData[i] = entry.getValue().get(i);
			}
		}
		MCCMNC mccMNC = ef.createMCCMNC(cellData);
		assertEquals(mccMNC.getClass(), MCCMNC.class);
		assertEquals("Guadeloupe-France", mccMNC.getCountry());
	}

	public Map<Integer, List<HSSFCell>> getWorkBookData(String filename, int sheetNum) throws IOException {
		FileInputStream inputStream = new FileInputStream(filename);
		workBook = new HSSFWorkbook(inputStream);
		inputStream.close();
		workBookSheet = workBook.getSheetAt(sheetNum);

		Map<Integer, List<HSSFCell>> data = new HashMap<>();
		int i = 0;
		for (Row row : workBookSheet) {
			// if (row.getRowNum() != 0) {
			data.put(i, new ArrayList<HSSFCell>());
			List<HSSFCell> cells = data.get(i);
			for (Cell cell : row) {
				cells.add(cell.getColumnIndex(), (HSSFCell) cell);
			}
			i++;
		} // }
		return data;
	}
	
	@Test
	void entityManagerTest() {
		ef.setEm(em);
		assertEquals(ef.getEm(), em);
	}
}
