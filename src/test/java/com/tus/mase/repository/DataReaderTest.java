package com.tus.mase.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.persistence.EntityManager;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.tus.mase.data.EventCauseDAO;
import com.tus.mase.data.EventDAO;
import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.data.MCCMNCDAO;
import com.tus.mase.data.UEClassDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.EventCause;
import com.tus.mase.model.FailureClass;
import com.tus.mase.model.MCCMNC;
import com.tus.mase.model.UEClass;

class DataReaderTest {

	private String filename = "src/test/resources/validTestData.xls";
	private HSSFWorkbook workBook;
	private HSSFSheet workBookSheet;
	private HSSFCell[] cellData;
	private DataReader dataReader;
	private Validation validate;
	private EntityFactory ef;
	private EventDAO eventDAO;
	private EventCauseDAO eventCauseDAO;
	private FailureClassDAO failureClassDAO;
	private UEClassDAO ueClassDAO;
	private MCCMNCDAO mccMncDAO;
	private EntityManager em;
	private Event eventObj = null;
	private EventCause eventCauseObj = null;
	private FailureClass failureClassObj = null;
	private UEClass ueClassObj = null;
	private MCCMNC mccMncObj = null;
	private InputStream inputStream;

	@BeforeEach
	void setUp() throws FileNotFoundException {
		dataReader = new DataReader();
		validate = new Validation();
		ef = mock(EntityFactory.class);
		em = mock(EntityManager.class);
		dataReader.setValidate(validate);
		dataReader.setEntityFactory(ef);
		eventDAO = mock(EventDAO.class);
		eventCauseDAO = mock(EventCauseDAO.class);
		failureClassDAO = mock(FailureClassDAO.class);
		ueClassDAO = mock(UEClassDAO.class);
		mccMncDAO = mock(MCCMNCDAO.class);
		ef = mock(EntityFactory.class);
		File file = new File(filename);
		inputStream = new FileInputStream(file);
	}

	@Test
	void validateDataReadEventTest() throws IOException {
		workBook = dataReader.getWorkBook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.BASE_DATA.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setEventDAO(eventDAO);
		eventDAO.setEntityManager(em);
		dataReader.readerExecute(workBook, Sheets.BASE_DATA);
		verify(eventDAO, new Times(4)).save(eventObj);
	}

	@Test
	void invalidEventDataReadTest() throws IOException {
		String invalidfile = "src/test/resources/invalidTestData.xls";
		InputStream invalid_dataStream = new FileInputStream(invalidfile);
		workBook = dataReader.getWorkBook(invalid_dataStream);
		workBookSheet = workBook.getSheet(Sheets.BASE_DATA.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setEntityFactory(ef);
		dataReader.readerExecute(workBook, Sheets.BASE_DATA);
		for (int i = 0; i < cellData.length; i++) {
			verify(ef, new Times(1)).createEvent(cellData);
		}
	}

	@Test
	void validateDataReadEventCauseTest() throws IOException {
		workBook = dataReader.getWorkBook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.EVENT_CAUSE_TABLE.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setEventCauseDAO(eventCauseDAO);
		eventCauseDAO.setEntityManager(em);
		dataReader.readerExecute(workBook, Sheets.EVENT_CAUSE_TABLE);
		verify(eventCauseDAO, new Times(3)).save(eventCauseObj);
	}

	@Test
	void validateDataReadFailureClassTest() throws IOException {
		workBook = dataReader.getWorkBook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.FAILURE_CLASS_TABLE.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setFailureClassDAO(failureClassDAO);
		failureClassDAO.setEntityManager(em);
		dataReader.readerExecute(workBook, Sheets.FAILURE_CLASS_TABLE);
		verify(failureClassDAO, new Times(4)).save(failureClassObj);
	}

	@Test
	void validateDataReadUEClassTest() throws IOException {
		workBook = dataReader.getWorkBook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.UE_TABLE.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setUeClassDAO(ueClassDAO);
		ueClassDAO.setEntityManager(em);
		dataReader.readerExecute(workBook, Sheets.UE_TABLE);
		verify(ueClassDAO, new Times(2)).save(ueClassObj);
	}

	@Test
	void validateDataReadMCCMNCTest() throws IOException {
		workBook = dataReader.getWorkBook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.MCC_MNC_TABLE.getSheetName());
		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
		}
		dataReader.setMccMncDAO(mccMncDAO);
		mccMncDAO.setEntityManager(em);
		dataReader.readerExecute(workBook, Sheets.MCC_MNC_TABLE);
		verify(mccMncDAO, new Times(4)).save(mccMncObj);
	}

	@Test
	void validateSetFilePAthTest() {
		dataReader.setFilePath("inputfolder/AIT Group Project 2022- Dataset 3B.xls");
		assertEquals("inputfolder/AIT Group Project 2022- Dataset 3B.xls", dataReader.getFilePath());
	}
}
