package com.tus.mase.repository;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.BufferedWriter;
import java.io.IOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

class WriteToLogTest {
	private WriteToLog writeToLog;
	private String invalidData;
	private BufferedWriter bw;

	@BeforeEach
	void setUp() {
		writeToLog = new WriteToLog();
		bw = mock(BufferedWriter.class);
		writeToLog.setBw(bw);
	}

	@Test
	void test() throws IOException {
		writeToLog.createLogEntry(invalidData, bw);
		verify(bw, new Times(1)).append(invalidData);
		verify(bw, new Times(1)).newLine();
		verify(bw, new Times(1)).close();
	}
}
