package com.tus.mase.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FolderWatcherTest {

	private FolderWatcher folderWatcher;
	private WatchService watchService;
	@SuppressWarnings("unused")
	private Path path;

	@BeforeEach
	void setUp() throws IOException {
		folderWatcher = new FolderWatcher();
		watchService = mock(WatchService.class);
		path = mock(Path.class);
	}

	@Test
	void folderWatcherIsCancelledTest() throws IOException {
		folderWatcher.stopService();
		assertEquals(true, folderWatcher.isCancelled());
	}

	@Test
	void folderStopFileWatcherTest() throws IOException {
		folderWatcher.stopFileWatcher();
		assertEquals(null, folderWatcher.getWatchService());
		folderWatcher.setWatchService(watchService);
		folderWatcher.stopFileWatcher();
		assertEquals(watchService, folderWatcher.getWatchService());
	}
}
