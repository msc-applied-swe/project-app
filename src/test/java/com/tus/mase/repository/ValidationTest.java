package com.tus.mase.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.repository.Sheets;
import com.tus.mase.repository.Validation;

class ValidationTest {

	private Validation validate;
	private String validDataPath = "src/test/resources/validTestData.xls";
	private String invalidDataPath = "src/test/resources/invalidTestData.xls";
	private static String FAIL_DESCRIPTION = "Data is null";
	private FileInputStream inputStream;
	private HSSFWorkbook workBook;
	private HSSFSheet workBookSheet;
	private HSSFCell[] cellData;

	@BeforeEach
	void setUp() {
		validate = new Validation();
		cellData = null;
	}

	@Test
	void validateValidEventDataTest() throws IOException {
		inputStream = getInputStream(validDataPath);
		workBook = new HSSFWorkbook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.BASE_DATA.getSheetName());

		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
			assertTrue(validate.validateData(cellData, workBookSheet));
		}
	}

	@Test
	void validateNotValidEventDataTest() throws IOException {
		inputStream = getInputStream(invalidDataPath);
		workBook = new HSSFWorkbook(inputStream);
		workBookSheet = workBook.getSheet(Sheets.BASE_DATA.getSheetName());

		for (Row row : workBookSheet) {
			cellData = new HSSFCell[row.getLastCellNum()];
			for (Cell cell : row) {
				cellData[cell.getColumnIndex()] = (HSSFCell) cell;
			}
			assertFalse(validate.validateData(cellData, workBookSheet));
		}
	}

	@Test
	void validateFailDescription() {
		validate.setDescription(FAIL_DESCRIPTION);
		assertEquals(FAIL_DESCRIPTION, validate.getDescription());
	}

	public FileInputStream getInputStream(String filename) throws FileNotFoundException {
		FileInputStream inputStream = new FileInputStream(filename);
		return inputStream;
	}
}
