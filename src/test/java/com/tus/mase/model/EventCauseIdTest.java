package com.tus.mase.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.model.EventCauseId;

import junit.framework.Assert;

public class EventCauseIdTest {
	
	private int causeCode=12;
	private int eventId=14;
	EventCauseId eventCauseId1;
	EventCauseId eventCauseId2;
	
	@BeforeEach
	public void initTestCase() {
	
	eventCauseId1 = new EventCauseId(causeCode, eventId);
	eventCauseId2 = new EventCauseId(causeCode, eventId);
	}
	
	@Test
	public void testEventCauseCode() {
		eventCauseId1.setCauseCode(causeCode);
		assertEquals(causeCode, eventCauseId1.getCauseCode());
		
	}
	
	
	@Test
	public void testEventId() {
		
		eventCauseId1.setEventId(eventId);
		assertEquals(eventId, eventCauseId1.getEventId());
	}
	
	@Test
	public void testEquals() {
		assertEquals(eventCauseId1, eventCauseId2);
	}

	@Test
	public void testHashcode() {
		assertTrue(eventCauseId1.hashCode()==eventCauseId2.hashCode());
		eventCauseId1 = new EventCauseId(causeCode, eventId);
		eventCauseId2 = new EventCauseId(causeCode, eventId);
		assertEquals(eventCauseId1, eventCauseId1);
	}
	

}
