package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.model.Users;

class UsersTest {

	Users user;
	@BeforeEach
	void setUp() throws Exception {
		user = new Users();
	}

	@Test
	void testConstructor() {
		user = new Users("username","password");
		assertEquals(user.getUsername(), "username");
		assertEquals(user.getPassword(), "password");
	}
	
	@Test
	void testSetName() {
		String name = "test";
		user.setUsername(name);
		assertEquals(user.getUsername(), name);
	}
	
	@Test 
	void testGetName() {
		user.setUsername("test");
		assertEquals(user.getUsername(), "test");
	}
	
	@Test
	void testSetId() {
		user.setUserId(1);
		assertEquals(user.getUserId(), 1);
	}
	
	@Test
	void testGetId() {
		user.setUserId(1);
		assertEquals(1, user.getUserId());
	}

	@Test
	void testSetPassword() {
		user.setPassword("password");
		assertEquals(user.getPassword(),"password");
	}
	
	@Test
	void testGetPassword() {
		user.setPassword("password");
		assertEquals(user.getPassword(), "password");
	}
	
	@Test
	void testSetEmployeeType() {
		user.setType("Administrator");
		assertEquals(user.getType(), "Administrator");
	}
	
	@Test
	void testGetEmployeeType() {
		user.setType("Administrator");
		assertEquals(user.getType(), "Administrator");
	}
	
	
}

