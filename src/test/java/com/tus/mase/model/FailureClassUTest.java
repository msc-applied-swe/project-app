package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tus.mase.model.FailureClass;

class FailureClassUTest {
	private static final int FAILURE_CLASS = 0;
	private static final String DESCRIPTION = "EMERGENCY";
	private FailureClass fc = new FailureClass(1, "HIGH PRIORITY ACCESS");

	@Test
	void updateFailureClass_WithValidData_ReturnsUpdatedFailureClass() {
		fc.setFailureClass(FAILURE_CLASS);
		assertEquals(FAILURE_CLASS, fc.getFailureClass());
	}

	@Test
	void updateFailureClassDescription_WithValidData_ReturnsUpdatedDescription() {
		fc.setDescription(DESCRIPTION);
		assertEquals(DESCRIPTION, fc.getDescription());
	}
}
