package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.*;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.model.Event;

class EventUTest {
	private static final String DATE_IN_STRING = "11-01-2020 17:15";
	private static Date DATE_TIME;
	private static final int EVENT_ID = 4098;
	private static final String FAILURE_CLASS = "1";
	private static final int UE_TYPE = 21060800;
	private static final int MARKET = 344;
	private static final int OPERATOR = 930;
	private static final int CELL_ID = 4;
	private static final int DURATION = 1000;
	private static final String CAUSE_CODE = "0";
	private static final String NE_VERSION = "11B";
	private static final BigInteger IMSI = new BigInteger("344930000000011");
	private static final BigInteger HIER3_ID = new BigInteger("4809532081614990000");
	private static final BigInteger HIER32_ID = new BigInteger("8226896360947470000");
	private static final BigInteger HIER321_ID = new BigInteger("1150444940909480000");
	private Event event;

	@BeforeEach
	void setUp() throws Exception {
		DATE_TIME = DateUtils.parseDate(DATE_IN_STRING, new String[] { "dd-MM-yyyy HH:mm" });
		event = new Event(DATE_TIME, 4090, "2", 21111111, 443, 950, 3, 1001, "2", "22c",
				new BigInteger("344900000000000"), new BigInteger("4809500000000000000"),
				new BigInteger("8226000000000000000"), new BigInteger("1150000000000000000"));
	}

//	@Test
//	void testEventReturnsToString() {
//		assertEquals(event.toString(), "Event [id=0, date=Sat Jan 11 17:15:00 GMT 2020, eventId=4090, failureClass=2, "
//				+ "UEType=21111111, market=443, operator=950, cellId=3, duration=1001, causeCode=2, neVersion=22c, IMSI=344900000000000, "
//				+ "HIER3_ID=4809500000000000000, HIER32_ID=8226000000000000000, HIER321_ID=1150000000000000000]");
//	}

	@Test
	void updateEventDate_WithValidData_ReturnsUpdatedDate() {
		event.setDate(DATE_TIME);
		assertEquals(DateUtils.truncate(event.getDate(), Calendar.SECOND),
				DateUtils.truncate(DATE_TIME, Calendar.SECOND));
	}

	@Test
	void updateEventId_WithValidData_ReturnsUpdatedeventId() {
		event.setEventId(EVENT_ID);
		assertEquals(EVENT_ID, event.getEventId());
	}

	@Test
	void updateEventFailureClass_WithValidData_ReturnsUpdatedEventFailureClass() {
		event.setFailureClass(FAILURE_CLASS);
		assertEquals(FAILURE_CLASS, event.getFailureClass());
	}

	@Test
	void updateEventUEType_WithValidData_ReturnsUpdatedEventUEType() {
		event.setUEType(UE_TYPE);
		assertEquals(UE_TYPE, event.getUEType());
	}

	@Test
	void updateEventMarket_WithValidData_ReturnsUpdatedEventMarket() {
		event.setMarket(MARKET);
		assertEquals(MARKET, event.getMarket());
	}

	@Test
	void updateEventOperator_WithValidData_ReturnsUpdatedEventOperator() {
		event.setOperator(OPERATOR);
		assertEquals(OPERATOR, event.getOperator());

	}

	@Test
	void updateEventCellId_WithValidData_ReturnsUpdatedEventCellId() {
		event.setCellId(CELL_ID);
		assertEquals(CELL_ID, event.getCellId());
	}

	@Test
	void updateEventDuration_WithValidData_ReturnsUpdatedEventDuration() {
		event.setDuration(DURATION);
		assertEquals(DURATION, event.getDuration());
	}

	@Test
	void updateEventCauseCode_WithValidData_ReturnsUpdatedEventCauseCode() {
		event.setCauseCode(CAUSE_CODE);
		assertEquals(CAUSE_CODE, event.getCauseCode());

	}

	@Test
	void updateEventNeVersion_WithValidData_ReturnsUpdatedEventNeVersion() {
		event.setNeVersion(NE_VERSION);
		assertEquals(NE_VERSION, event.getNeVersion());
	}

	@Test
	void updateEventIMSI_WithValidData_ReturnsUpdatedEventIMSI() {
		event.setIMSI(IMSI);
		assertEquals(IMSI, event.getIMSI());
	}

	@Test
	void updateEventHIER3_ID_WithValidData_ReturnsUpdatedEventHIER3_ID() {
		event.setHIER3_ID(HIER3_ID);
		assertEquals(HIER3_ID, event.getHIER3_ID());
	}

	@Test
	void updateEventHIER32_ID_WithValidData_ReturnsUpdatedEventHIER32_ID() {
		event.setHIER32_ID(HIER32_ID);
		assertEquals(HIER32_ID, event.getHIER32_ID());
	}

	@Test
	void updateEventHIER321_ID_WithValidData_ReturnsUpdatedEventHIER321_ID() {
		event.setHIER321_ID(HIER321_ID);
		assertEquals(HIER321_ID, event.getHIER321_ID());
	}
}