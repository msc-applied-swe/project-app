package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tus.mase.model.MCCMNC;

class MCCMNCUTest {
	private static final int MCC = 405;
	private static final int MNC = 5;
	private static final String COUNTRY = "India";
	private static final String OPERATOR = "Reliance Infocomm-IN";
	MCCMNC mccmnc = new MCCMNC(505, 90, "Australia", "Optus Ltd. AU");
	
	@Test
	void updateMCCMNC_WithValidData_ReturnsUpdatedMCC() {
		mccmnc.setMcc(MCC);
		assertEquals(MCC, mccmnc.getMcc());
	}
	
	@Test
	void updateMCCMNC_WithValidData_ReturnsUpdatedMNC() {
		mccmnc.setMnc(MNC);
		assertEquals(MNC, mccmnc.getMnc());
	}

	@Test
	void updateMCCMNC_WithValidData_ReturnsUpdatedCountry() {
		mccmnc.setCountry(COUNTRY);
		assertEquals(COUNTRY, mccmnc.getCountry());
	}
	
	@Test
	void updateMCCMNC_WithValidData_ReturnsUpdatedOPERATOR() {
		mccmnc.setOperator(OPERATOR);
		assertEquals(OPERATOR, mccmnc.getOperator());
	}
}
