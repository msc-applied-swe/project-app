package com.tus.mase.model;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class UEEventTest {
	
	
	private static final String MODEL = "G410";
	private static final String CAUSECODE = "8";
	private static final int EVENTID = 4097;
	private static final String DESCRIPTION = "RRC CONN SETUP-UE CAPABILITY ENQUIRY TIMEOUT";
	private static final BigInteger OCCURRENCES = new BigInteger("200");

	UEEvent uEEvent= new UEEvent(EVENTID,CAUSECODE, DESCRIPTION, MODEL, OCCURRENCES);
	
	@Test
	public void testModelUpdated() {
		uEEvent.setModel(MODEL);
		assertEquals(MODEL, uEEvent.getModel());
	}
	
	
	@Test
	public void testCauseCodeUpdated() {
		uEEvent.setCauseCode(CAUSECODE);
		assertEquals(CAUSECODE, uEEvent.getCauseCode());
	}
	
	@Test
	public void testEventIDUpdated() {
		uEEvent.setEventId(EVENTID);
		assertEquals(EVENTID, uEEvent.getEventId());
	}
	

	@Test
	public void testDescriptionUpdated() {
		uEEvent.setDescription(DESCRIPTION);
		assertEquals(DESCRIPTION, uEEvent.getDescription());
	}
	
	@Test
	public void testOccurrences() {
		uEEvent.setOccurrences(OCCURRENCES);
		assertEquals(OCCURRENCES, uEEvent.getOccurrences());
	}
	
}
