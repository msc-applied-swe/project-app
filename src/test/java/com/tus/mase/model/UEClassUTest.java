package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.model.UEClass;

class UEClassUTest {
	private static final long TAC = 100100;
	private static final String MARKETING = "G410";
	private static final String MANUFACTURER = "Mitsubishi";
	private static final String ACCESS_CAPABILITY = "GSM 1800, GSM 900";
	private static final String MODEL="G410";
	private static final String VENDORNAME="Mitsubishi";
	private static final String UETYPE="Handheld"; 
	private static final String OS="(null)";
	private static final String INPUTMODE="(null)";
	UEClass ueClass = new UEClass(101600, "ALCATEL OT-807A", "TCT Mobile Suzhou Limited", "GSM 1900, GSM850 (GSM800)", "G410", "Mitsibushi", "HandHeld", "Blackberry", "QWERTY");

	@Test
	void updateUEClass_WithValidData_ReturnsUpdatedTAC() {
		ueClass.setTAC(TAC);
		assertEquals(TAC, ueClass.getTAC());
	}

	@Test
	void updateUEClass_WithValidData_ReturnsUpdatedMarketing() {
		ueClass.setMarketing(MARKETING);
		assertEquals(MARKETING, ueClass.getMarketing());
	}

	@Test
	void updateUEClass_WithValidData_ReturnsUpdatedManufacturer() {
		ueClass.setManufacturer(MANUFACTURER);
		assertEquals(MANUFACTURER, ueClass.getManufacturer());
	}

	@Test
	void updateUEClass_WithValidData_ReturnsUpdatedAccessCapability() {
		ueClass.setAccessCapability(ACCESS_CAPABILITY);
		assertEquals(ACCESS_CAPABILITY, ueClass.getAccessCapability());
	}
	
	@Test
	void updateUEClass_WithValidData_ReturnsUpdatedModel() {
		ueClass.setAccessCapability(MODEL);
		assertEquals(MODEL, ueClass.getModel());
	}
	
	@Test
	void updateUEClass_WithValidData_ReturnsVendorName() {
		ueClass.setVendorName(VENDORNAME);;
		assertEquals(VENDORNAME, ueClass.getVendorName());
	}
	
	@Test
	void updateUEClass_WithValidData_ReturnsModel() {
		ueClass.setModel(MODEL);;
		assertEquals(MODEL, ueClass.getModel());
	}
	
	@Test
	void updateUEClass_WithValidData_ReturnsUEType() {
		ueClass.setUeType(UETYPE);
		assertEquals(UETYPE, ueClass.getUeType());
	}
	
	@Test
	void updateUEClass_WithValidData_ReturnsOS() {
		ueClass.setOs(OS);
		assertEquals(OS, ueClass.getOs());
	}
	
	@Test
	void updateUEClass_WithValidData_InputMode() {
		ueClass.setInputMode(INPUTMODE);
		assertEquals(INPUTMODE, ueClass.getInputMode());
	}
	
	
}
