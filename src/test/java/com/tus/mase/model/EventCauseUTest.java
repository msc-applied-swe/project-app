package com.tus.mase.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import com.tus.mase.model.EventCause;

class EventCauseUTest {
	private static final int CAUSE_CODE = 0;
	private static final int EVENT_ID = 4097;
	private static final String DESCRIPTION = "RRC CONN SETUP-SUCCESS";
	private EventCause eventCause = new EventCause(1, 4080, "RRC CONN SETUP-CELL UNAVAILABLE");

	@Test
	void updateEventCauseCode_WithValidData_ReturnsUpdatedCauseCode() {
		eventCause.setCauseCode(CAUSE_CODE);
		assertEquals(CAUSE_CODE, eventCause.getCauseCode());
	}

	@Test
	void updateEventCauseEventId_WithValidData_ReturnsUpdatedEventId() {
		eventCause.setEventId(EVENT_ID);
		assertEquals(EVENT_ID, eventCause.getEventId());
	}

	@Test
	void updateEventCauseDescription_WithValidData_ReturnsUpdatedDescription() {
		eventCause.setDescription(DESCRIPTION);
		assertEquals(DESCRIPTION, eventCause.getDescription());
	}
}
