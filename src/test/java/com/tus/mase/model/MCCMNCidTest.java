package com.tus.mase.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tus.mase.model.MCCMNCid;

public class MCCMNCidTest {
	
	private int mcc;
	private int mnc;
	MCCMNCid mCCMNCid1;
	MCCMNCid mCCMNCid2;
	
	@BeforeEach
	public void initTestCase() {
	
	mCCMNCid1 = new MCCMNCid(mcc, mnc);
	mCCMNCid2 = new MCCMNCid(mcc, mnc);
	
	}
	
	@Test
	public void testMCC() {
		
		mCCMNCid1.setMcc(mcc);
		assertEquals(mcc, mCCMNCid1.getMcc());
	}
	
	
	@Test
	public void testMNC() {
		
		mCCMNCid1.setMnc(mnc);
		assertEquals(mnc, mCCMNCid1.getMnc());
	}
	
	@Test
	public void testEquals() {
		assertEquals(mCCMNCid1, mCCMNCid2);
	}

	@Test
	public void testHashcode() {
		assertTrue(mCCMNCid1.hashCode()==mCCMNCid2.hashCode());
	}
	

}
