//let url = 'http://localhost:8080/project-app/rest/FailureClass';
let url = 'rest/FailureClass';

$(document).ready(function(){
    let renderArea = document.querySelector('select');

    $.ajax({
        url : url,
        type : "GET",
        contentType : "application/json",
        dataType : 'json',
        success : function renderSuggestions(data) {
            for(let i = 0; i < data.length; i++) {
                renderArea.insertAdjacentHTML('beforeend',
                    '<option value=' + data[i].failureClass + '>' + data[i].description + '</option>');
            }
        }
    });
});

function getCauseClass() {
    document.getElementById("errorArea").innerHTML = "";
    let causeClass = document.getElementById("causeClass");
    let causeClassVal = causeClass.value;
    if(!causeClassVal) {
        document.getElementById("errorArea").innerHTML = "Please select an option";
        return;
    }
	console.log(url + '/find/' + causeClassVal);
    $.ajax({
        url : url + '/find/' + causeClassVal,
        type : "GET",
        contentType : "application/json",
        dataType : 'json',
        success : renderList
    });
}

let renderList=function(data){
    list=data;
    $.each(list,function(index, projectdb){
        let code = projectdb[1];
        $('#table_body').append('<tr><td>'+projectdb[0]+'</td><td>'+code.replace('.0', '')+'</td></tr>');
    });
    $('#table_id').DataTable( {
        "language": {
            search: "Search All Data:",
            searchPlaceholder: "Search..."
        }
    });
};


//When the DOM is ready.
$(document).ready(function(){
    $('#resetButton').on('click', function() {
        window.location.reload();
    })
});