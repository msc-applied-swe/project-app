// The root URL for the RESTful services

//var rootURL = "http://localhost:8080/project-app/rest/Event";
var rootURL = "rest/Event";

let draw = false;


var findAll=function() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
};


var findByDates = function(startDate, endDate) {
	
	var st = startDate.replace('T', ' ')
	var et = endDate.replace('T', ' ')
	
	if (st == "Invalid Date or Time") {
		alert("The 'From' date/time field cannot be empty.");
		$("#fromDateTime").focus();
		return;
	}
	if (et == "Invalid Date or Time") {
		alert("The 'To' date field cannot be empty.");
		$("#toDateTime").focus();
		return;
	}
	
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + st + '/' + et,
		dataType: "json",
		success: function(data) {
			renderList(data);
		}
	});
};

let renderList=function(data){
	list=data;
	var str = "";
	str += "<h5>Total Call Failures Within This Specific Timeframe: " + data.length + "<h5>";
	document.getElementById("resultSpace").innerHTML = str;
//	$.each(list,function(index, projectdb){
//		$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+new Date(projectdb.date)+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td></tr>');
//		});
		$.each(list, function(index, projectdb) {
		$('#table_body').append('<tr><td>' + projectdb[0] + '</td><td>' + new Date(projectdb[1]) + '</td><td>' + projectdb[2]+ '</td><td>' + projectdb[3]  +'</td></tr>');
	});
	
		$('#table_id').DataTable( {
  		"language": {
        search: "Search All Data:   ",
        searchPlaceholder: "Search..."
    	}
} );	  
		  const table = $("#table_id").DataTable();
		  // get table data
		  const tableData = getTableData(table);
		  // create Highcharts
		  createHighcharts(tableData);
		  // table events
		  setTableEvents(table);  

};

function getTableData(table) {
  const dataArray = [],
    imsiArray = [],
    causeCodeArray = [],
    eventIdArray = [];
 
  // loop table rows
  table.rows({ search: "applied" }).every(function() {
    const data = this.data();
    imsiArray.push(data[0]);
    causeCodeArray.push(parseInt(data[3].replace(/\,/g, "")));
    eventIdArray.push(parseInt(data[2].replace(/\,/g, "")));
  });
 
  // store all data in dataArray
  dataArray.push(imsiArray, causeCodeArray, eventIdArray);
 
  return dataArray;
}


function createHighcharts(data) {
  Highcharts.setOptions({
    lang: {
      thousandsSep: ""
    }
  });
 
  Highcharts.chart("chart", {
    title: {
      text: "IMSI List With Call Failures Within A Specific Timeframe"
    },
    subtitle: {
      text: "Data from Call Failures Query"
    },
    xAxis: [
      {
        categories: data[0],
        labels: {
          rotation: -25
        }
      }
    ],
    yAxis: [
      {
        // first y-axis
        title: {
          text: "Event ID (2020)"
        },
        min: 4000
      },

      {
        // secondary y-axis
        title: {
          text: "Cause Code (2020)"
        },
        min: 0,
        opposite: true
      }
    ],
    series: [
      {
        name: "Event ID (2020)",
        color: "#0071A7",
        type: "column",
        data: data[1],
        tooltip: {
          valueSuffix: ""
        }
      },
      {
        name: "Cause Code (2020)",
        color: "#FF404E",
        type: "spline",
        data: data[2],
        yAxis: 1
      }
    ],
    tooltip: {
      shared: true
    },
    legend: {
      backgroundColor: "#ececec",
      shadow: true
    },
    credits: {
      enabled: false
    },
    noData: {
      style: {
        fontSize: "16px"
      }
    }
  });
}

 
function setTableEvents(table) {
  // listen for page clicks
  table.on("page", () => {
    draw = true;
  });
 
  // listen for updates and adjust the chart accordingly
  table.on("draw", () => {
    if (draw) {
      draw = false;
    } else {
      const tableData = getTableData(table);
      createHighcharts(tableData);
    }
  });
}



//When the DOM is ready.
$(document).ready(function(){

	$(document).on("click", "#submitButton", function() {
	findByDates($('#fromDateTime').val(), $('#toDateTime').val());
	});

	$('#resetButton').on('click', function() {
    window.location.reload();
})

});
