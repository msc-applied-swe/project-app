
function getFailuresByDate() {

	//var url = 'http://localhost:8080/project-app/rest/Event/search/' + startDate + '/' + endDate;
	var url = 'rest/Event/search/' + startDate + '/' + endDate;

	var startDate = $("#fromDateTime").date().format("YYYY-MM-DD HH:mm");
	var endDate = $("#toDateTime").date().format("YYYY-MM-DD HH:mm");

	if (fromDateTime == "Invalid Date or Time") {
		alert("The 'From' date/time field cannot be empty.");
		$("#fromDateTime").focus();
		return;
	}
	if (toDateTime == "Invalid Date or Time") {
		alert("The 'To' date field cannot be empty.");
		$("#toDateTime").focus();
		return;
	}

	var JSONObject = {
		"Date1" : fromDateTime,
		"Date2" : toDateTime
	};
	JSONObject = JSON.stringify(JSONObject);

	$.ajax({
		url : url,
		type : "POST",
		data : JSONObject,
		contentType : "application/json",
		dataType : 'json',
		success : populateTable
	});
}

function populateTable(data) {
	var t = $('#datatable-1').DataTable();
	t.clear();

	if (data.length == 0) {
		alert("No results for lookup");
	}

	var i;
	for (i = 0; i < data.length; i++) {
		t.row.add([ data[i] ]);
	}
	t.draw();
}

