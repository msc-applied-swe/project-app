var rootURL = 'rest/Event';

//When the DOM is ready.
$(document).ready(function(){

	//getIMSIs();

	
});

let manualEntry = document.getElementById("IMSI");
let dropDownEntry = document.getElementById("causeClass");
let errorArea = document.getElementById("errorArea");
manualEntry.value = "";

function getMenu(){
	manualEntryV = manualEntry.value;
	dropDownEntryV = dropDownEntry.value;  
	errorArea.innerHTML = "";
	
	let item;
	if(!manualEntryV && !dropDownEntryV) {
		errorArea.innerHTML = "Please provide an IMSI";
		return;	
	}
	
	if(!manualEntryV) {
		item = dropDownEntryV;
	} else if (!dropDownEntryV) {
		item = manualEntryV; 
	}
	
	if(manualEntryV && dropDownEntryV) {
		item = dropDownEntryV;
	}
	
	

var url = 'rest/Event'
	 var $table = $('#userTable')
	
	$.ajax({
		url: url+"/searchCauseCode/"+encodeURI(item), 
		type: 'GET', 
		contentType: 'application/json',
		dataType: 'json',
		async: false,
		success: function(data, xhr) {
		
			renderList(data);
			
		},
			
	});

}


var renderList=function(data){
	console.table(data);
	list=data;
	var str = "";
	str += "<p>Total unique rows found: " + data.length + "<p>";
	$("#table_body").empty();
	document.getElementById("resultSpace").innerHTML = str;
	$.each(list,function(index, causeCode){
		//$('#table_body').append('<tr><td>'+causeCode+'</td></tr>');
		$('#table_body').append('<tr><td>'+causeCode[1]+'</td><td>'+ causeCode[0]+'</td></tr>');
		});  
	

};


function getIMSIs(){
var url = 'rest/Event'
$.ajax({
		url: url+"/getAllIMSI", 
		type: 'GET', 
		contentType: 'application/json',
		dataType: 'json',
		async: false,
		success: showIMSI,
		/*success: function(data, xhr) {
			
			$.each(data,function(index, IMSI){
			$('#selectIMSI').append('<a class="dropdown-item" href="#" onClick="getMenu(this)">'+IMSI+'</a>');
			 
			});
		},*/
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 
	        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
	    }
			
	});

}

$(document).ready(function(){
	$('#resetButton').on('click', function() {
        window.location.reload();
    })
    let renderArea = document.querySelector('select');
	var url = 'rest/Event';
    $.ajax({
        url : url+"/getAllIMSI",
        type : "GET",
        contentType : "application/json",
        dataType : 'json',
        success : function renderSuggestions(data) {
		
            for(let i = 0; i < data.length; i++) {
                renderArea.insertAdjacentHTML('beforeend',
                    '<option value=' + data[i] + '>' + data[i] + '</option>');
            }
        }
    });
});



