// The root URL for the RESTful services
var rootURL = "http://localhost:8080/project-app/rest/EventError";
var findAll=function() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
};


var renderList=function(data){
	list=data;
	$.each(list,function(index, projectdb){
		$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+projectdb.date+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.uetype+'</td><td>'+projectdb.cellId+'</td><td>'+projectdb.eventId+'</td><td>'+projectdb.market+'</td><td>'+projectdb.failureClass+'</td><td>'+projectdb.neVersion+'</td><td>'+projectdb.operator+'</td><td>'+projectdb.description+'</td></tr>');
		});  
		$('#table_id').DataTable( {
  		"language": {
        search: "Input IMSI",
        searchPlaceholder: "Search..."
    }
} );

};

//When the DOM is ready.
$(document).ready(function(){
	findAll();
});
