// The root URL for the RESTful services
var rootURL = "rest/Event";
var findAll=function() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
};

var findById = function(imsi) {
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + imsi,
		dataType: "json",
		success: function(data) {
			renderList(data);
		}
	});
}

var formatTotalDuration = function(numberOfSeconds) {
	var numberOfHours = Math.floor(numberOfSeconds/3600);
	remainingSeconds = numberOfSeconds - (numberOfHours*3600);
	var numberOfMins = Math.floor(remainingSeconds/60);
	remainingSeconds -= numberOfMins*60;
	var formattedTotalDuration = numberOfHours + " hr " + numberOfMins + " min " + remainingSeconds + " s";
	return formattedTotalDuration    
}

var renderList=function(data){
	var totalTime = 0;
	list=data;
	var str = "";
	$.each(list,function(index, projectdb){
		$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td><td>'+projectdb.duration+'</td></tr>');
		totalTime += projectdb.duration;
		});  
		str += "<h5>Total Call Failures For This IMSI: " + data.length + "<h5>";
		str += "<h5>Total duration for all failures: " + formatTotalDuration(totalTime) + "<h5>";
		document.getElementById("resultSpace").innerHTML = str;
		$('#table_id').DataTable( {
  		"language": {
        search: "Search All Data",
        searchPlaceholder: "Search..."
    }
} );

};

//When the DOM is ready.
$(document).ready(function(){

	$(document).on("click", "#submitButton", function() {
		findById($('#imsiNumber').val());
	});

	$('#resetButton').on('click', function() {
    window.location.reload();
})

});

