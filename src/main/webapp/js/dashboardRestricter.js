/**
 * This file is loaded at the top and bottom of the dashboard page which significantly 
improves performance and reduces 'peeking' at the admin-view dashboard 
 */

var type = sessionStorage.getItem("type");
		if(!type) {
			window.location.replace("index.html");
		}	

function triggerLogout() {
	sessionStorage.clear();
	sessionStorage.clear;
	window.location.replace("index.html");
}

$(document).ready(function(){
	var username = sessionStorage.getItem("username");
	var displayUser = document.getElementById("signedInUser");
	console.log(username);
	displayUser.innerHTML = username;
});

$(document).ready(function(){
if(type == "customer service rep") {
	var path = window.location.pathname;
	var page = path.split("/").pop();
	if(page == 'dashboard.html'){
		//main page
	document.getElementById("adminImport").style.display="none";
	document.getElementById("adminProfiles").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	document.getElementById("admin").style.display="none";
	document.getElementById("networkManagerCard").style.display="none";	
	document.getElementById("supportEngineerCard").style.display="none";
	document.getElementById("networkManagerCardIMSI").style.display="none";
	document.getElementById("networkManagerCardFailures").style.display="none";
	document.getElementById("topTenCombinations").style.display="none";
	document.getElementById("supportEngineerCardPhone").style.display="none";
	document.getElementById("supportEngineerIMSICauseCode").style.display="none";
	}
	
	//side bar 
	document.getElementById("importSidebar").style.display="none";
	document.getElementById("IMSITimeframeSidebar").style.display="none";
	document.getElementById("IMSICountSidebar").style.display="none";
	document.getElementById("phoneFailureSidebar").style.display="none";
	document.getElementById("topTenFailureSidebar").style.display="none";
	document.getElementById("topTenCombinationsSidebar").style.display="none";
	document.getElementById("profileSidebar").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	document.getElementById("query_IMSI_CauseClass").style.display="none";
	document.getElementById("query_model").style.display="none";
}

if(type == "support engineer") {
	var path = window.location.pathname;
	var page = path.split("/").pop();
	if(page == 'dashboard.html'){
	document.getElementById("adminImport").style.display="none";
	document.getElementById("adminProfiles").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	document.getElementById("admin").style.display="none";
	document.getElementById("networkManagerCard").style.display="none";	
	document.getElementById("networkManagerCardIMSI").style.display="none";
	document.getElementById("networkManagerCardFailures").style.display="none";
	document.getElementById("topTenCombinations").style.display="none";
	}
	//side bar 
	document.getElementById("importSidebar").style.display="none";
	document.getElementById("IMSICountSidebar").style.display="none";
	document.getElementById("topTenFailureSidebar").style.display="none";
	document.getElementById("profileSidebar").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	document.getElementById("query_model").style.display="none";
	document.getElementById("topTenCombinationsSidebar").style.display="none";
}



if(type == "network management engineer") {
	var path = window.location.pathname;
	var page = path.split("/").pop();
	if(page == 'dashboard.html'){
	document.getElementById("adminImport").style.display="none";
	document.getElementById("adminProfiles").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	document.getElementById("admin").style.display="none";
	}
	//side bar
	document.getElementById("importSidebar").style.display="none"; 
	document.getElementById("profileSidebar").style.display="none";
	document.getElementById("adminSidebar").style.display="none";
	}
});
