//let url = 'http://localhost:8080/project-app/rest/Event/combinations';

let url = 'rest/Event/combinations';
let startDate;
let endDate;

document.getElementById("fromDateTime").addEventListener("change", function() {
    var input = this.value;
    var month = String(new Date(input).getMonth()+1).padStart(2, '0');
    var year = String(new Date(input).getFullYear());
    var date = String(new Date(input).getDate()).padStart(2, '0');
    var hour = String(new Date(input).getHours()).padStart(2, '0');;
    var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
    startDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
});

document.getElementById("toDateTime").addEventListener("change", function() {
    var input = this.value;
    var month = String(new Date(input).getMonth()+1).padStart(2, '0');
    var year = String(new Date(input).getFullYear());
    var date = String(new Date(input).getDate()).padStart(2, '0');
    var hour = String(new Date(input).getHours()).padStart(2, '0');;
    var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
    endDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
});


function getTopTen() {
    document.getElementById("errorAreaStartDate").innerHTML = "";
    document.getElementById("errorAreaEndDate").innerHTML = "";

    if (!startDate) {
        document.getElementById("errorAreaStartDate").innerHTML = "Please provide a start date";
        return;
    }
    if (!endDate) {
        document.getElementById("errorAreaEndDate").innerHTML = "Please provide an end date";
        return;
    }


    $.ajax({
        url: url + '/' + startDate + '/' + endDate,
        type: "GET",
        contentType: "application/json",
        dataType: 'json',
        success: renderList
    });
}

let renderList = function(data) {
    list=data;
    output =[];
   $.each(list,function(index, projectdb){
   	e={'count':projectdb[0],'market':projectdb[1],'operator':projectdb[2],'cellId':projectdb[3]}
   	output.push(e);
   });
   
   console.log(output);
   
   	total_failures = 0;
    $.each(output,function(index, projectdb){
   		
    	total_failures+=projectdb.count;
        //$('#table_body').append('<tr><td>'+projectdb[0]+'</td><td>'+projectdb[1]+'</td><td>'+projectdb[2]+'</td><td>'+projectdb[3]+'</td></tr>');
        $('#table_body').append('<tr><td>'+projectdb.count+'</td><td>'+projectdb.market+'</td><td>'+projectdb.operator+'</td><td>'+projectdb.cellId+'</td></tr>');
    });
    $('#table_id').DataTable( {
        "language": {
            search: "Search All Data:",
            searchPlaceholder: "Search..."
        },
        "order" : [[0, "desc" ]]
    });
    
    const table = $("#table_id").DataTable();
			
					 // get table data
				  const tableData = getTableData(table, total_failures);
				 
				  // create Highcharts
				  createHighcharts(tableData);
				  createHighchartsForFailurePercentage(tableData);
				  // table events
				  setTableEvents(table);   
    
}
//When the DOM is ready.
$(document).ready(function(){
    $('#resetButton').on('click', function() {
        window.location.reload();
    })
});

function getTableData(table, total_failures) {
  const dataArray = [],
  	FrequencyArray = [],
  	MarketArray = [],
  	OperatorArray = [],
  	CellIDArray = [],
  	Operator_CellIdArray = [],
  	Percentage_of_call_failures = []
  	
 
  // loop table rows
  table.rows({ search: "applied" }).every(function() {
    const data = this.data();
    FrequencyArray.push(data[0]);
    Operator_CellIdArray.push('Operator '+ data[2]+' and Cell Id '+ data[3]);
    Percentage_of_call_failures.push((data[0] / total_failures) * 100);
    
    
  });
 
  // store all data in dataArray
  dataArray.push(FrequencyArray, Operator_CellIdArray, Percentage_of_call_failures);
 
  return dataArray;
}




function setTableEvents(table) {
  // listen for page clicks
  table.on("page", () => {
    draw = true;
  });
 
  // listen for updates and adjust the chart accordingly
  table.on("draw", () => {
    if (draw) {
      draw = false;
    } else {
      const tableData = getTableData(table);
      createHighcharts(tableData);
      createHighchartsForFailurePercentage(tableData);
    }
  });
}


function createHighcharts(data) {
console.log(data[1]);
intArray  = data[0].map(Number);
  Highcharts.chart('TopTenChart', {

    title: {
        text: 'Top 10 Market/Operator/Cell ID combinations'
    },

    //subtitle: {
    //  text: 'Number of call failures'
    //},

    yAxis: {
        title: {
            text: 'Number of call failures'
        }
    },

    xAxis: {
    title: {
            text: 'Operator & CellID'
        },
        categories: data[1],
        labels: {
          rotation: -25
        }
      },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
           
        }
    },

    series: [{
        name: 'Number of Call Failures',
        data: intArray
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
}


function createHighchartsForFailurePercentage(data) {

intArray  = data[2].map(Number);
  Highcharts.chart('FailurePercentageChart', {

    title: {
        text: 'Top 10 Call Failures'
    },

    //subtitle: {
    //  text: 'Percentage of call failures'
    //},

    yAxis: {
        title: {
            text: 'Percentage of Call Failures'
        }
    },

    xAxis: {
    title: {
            text: 'Operator & CellID'
        },
        categories: data[1],
        labels: {
          rotation: -25
        }
      },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
           
        }
    },

    series: [{
        name: 'Percentage of Call Failures',
        data: intArray
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
}


