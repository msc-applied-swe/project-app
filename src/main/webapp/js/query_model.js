var rootURL = 'rest/Event';

//When the DOM is ready.
$(document).ready(function(){

	getModels();

	
});

function getMenu(e){
	

var item = $(e).text()
var url = 'rest/UEClass'
	 var $table = $('#userTable')
	
	$.ajax({
		url: url+"/model/getAll/"+encodeURI(item), 
		type: 'GET', 
		contentType: 'application/json',
		dataType: 'json',
		async: false,
		success: function(data, xhr) {
			renderList(data);
			
		},
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 
	        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
	    }
			
	});

}


var renderList=function(data){
	list=data;
    let temp;
	var str = "";
	str += "<p>Total unique rows found: " + data.length + "<p>";
	$("#table_body").empty();
	document.getElementById("resultSpace").innerHTML = str;
    $.each(list,function(index, projectdb){
        //We convert the object to an array here
        temp = Object.values(projectdb);
		//$('#table_body').append('<tr><td>'+projectdb.model+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td><td>'+projectdb.description+'</td><td>'+projectdb.occurrences+'</td></tr>');
        $('#table_body').append('<tr><td>'+temp[0]+'</td><td>'+temp[1]+'</td><td>'+temp[2]+'</td><td>'+temp[3]+'</td><td>'+temp[4]+'</td></tr>');
		});
		
		
	const table = $("#table_id").DataTable();
	
			 // get table data
		  const tableData = getTableData(table);
		  // create Highcharts
		  createHighcharts(tableData);
		  // table events
		  setTableEvents(table);   
	

};


function getModels(){
var url = 'rest/UEClass'
$.ajax({
		url: url+"/model/getAll/", 
		type: 'GET', 
		contentType: 'application/json',
		dataType: 'json',
		async: false,
		success: function(data, xhr) {
			
			$.each(data,function(index, model){
			$('#selectModel').append('<a class="dropdown-item" href="#" onClick="getMenu(this)">'+model+'</a>');
			 
			});
		 
		},
	    error: function(XMLHttpRequest, textStatus, errorThrown) { 
	        alert("Status: " + textStatus); alert("Error: " + errorThrown); 
	    }
			
	});

}


function getTableData(table) {
  const dataArray = [],
  	modelArray = [],
    countArray = [],
    causeCodeArray = [],
    eventIdArray = [],
    descriptionArray = []
 
  // loop table rows
  table.rows({ search: "applied" }).every(function() {
    const data = this.data();
    modelArray.push(data[0]);
    causeCodeArray.push(parseInt(data[1].replace(/\,/g, "")));
    eventIdArray.push(parseInt(data[2].replace(/\,/g, "")));
    descriptionArray.push(data[3]);
    countArray.push(data[4]);
  });
 
  // store all data in dataArray
  dataArray.push(modelArray, causeCodeArray, eventIdArray, descriptionArray, countArray);
 
  return dataArray;
}




function setTableEvents(table) {
  // listen for page clicks
  table.on("page", () => {
    draw = true;
  });
 
  // listen for updates and adjust the chart accordingly
  table.on("draw", () => {
    if (draw) {
      draw = false;
    } else {
      const tableData = getTableData(table);
      createHighcharts(tableData);
    }
  });
}


function createHighcharts(data) {

intArray  = data[4].map(Number);
  Highcharts.chart('phoneFailureChart', {

    title: {
        text: 'Number of Phone Failures'
    },

    subtitle: {
        text: 'Count of occurrences of Phone Failures by Unique Cause Code and Event ID'
    },

    yAxis: {
        title: {
            text: 'Occurrences'
        }
    },

    xAxis: {
        categories: data[3],
        labels: {
          rotation: -25
        }
      },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
           
        }
    },

    series: [{
        name: 'No. of Occurrences',
        drilldown: 'Event ID',
        data: intArray
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
}



