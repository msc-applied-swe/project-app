//var rootURL = "http://localhost:8080/project-app/rest/Event";
var rootURL = "rest/Event";
let startDate;
let endDate;
let draw = false;

var findAll=function() {
	console.log('findAll');
	$.ajax({
		type: 'GET',
		url: rootURL,
		dataType: "json", // data type of response
		success: renderList
	});
};



var topTenIMSIFailures = function(startDate, endDate) {
	
	var st = startDate.replace('T', ' ')
	var et = endDate.replace('T', ' ')
	
	if (st == "Invalid Date or Time") {
		alert("The 'From' date/time field cannot be empty.");
		$("#fromDateTime").focus();
		return;
	}
	if (et == "Invalid Date or Time") {
		alert("The 'To' date field cannot be empty.");
		$("#toDateTime").focus();
		return;
	}

	$.ajax({
		type: 'GET',
		url: rootURL + '/search/IMSI/' + st + '/' + et,
		contentType : "application/json",
		dataType: "json",
		success: function(data) {
			renderList(data);
		}
	});
};


var renderList=function(data){
    list=data;
    var str = "";
	str += "<p>Total Call Failures Within This Specific Timeframe: " + data.length + "<p>";
	document.getElementById("resultSpace").innerHTML = str;
    $.each(list,function(index, data) {
            $('#table_body').append('<tr><td>' + data[0] + '</td><td>' + data[1] + '</td></tr>');
        });

    $('#table_id').DataTable( {
        "language": {
            search: "Search All Data:",
            searchPlaceholder: "Search..."
        },
        /*
        We are forcing DataTable to order the items by the second returned columned
        (which is the Count value) descending as it defaults to IMSI ordering if we don't
         */
       "order" : [[1, "desc" ]]
    });

    	  const table = $("#table_id").DataTable();
		  // get table data
		  const tableData = getTableData(table);
		  // create Highcharts
		  createHighcharts(tableData);
		  // table events
		  setTableEvents(table);  

};


function getTableData(table) {
  const dataArray = [],
    imsiArray = [],
    countArray = [];
 
  // loop table rows
  table.rows({ search: "applied" }).every(function() {
    const data = this.data();
    imsiArray.push(data[0]);
    countArray.push(parseInt(data[1].replace(/\,/g, "")));
  });
 
  // store all data in dataArray
  dataArray.push(imsiArray, countArray);
 
  return dataArray;
}


function createHighcharts(data) {
  Highcharts.setOptions({
    lang: {
      thousandsSep: ""
    }
  });
 
  Highcharts.chart("chart", {
  	
  	chart: {
  		type: 'column'
  	},


    title: {
      text: "Count of Call Failures per IMSI Within Specified Timeframe"
    },

    subtitle: {
      text: "Data from Call Failures Query"
    },

    xAxis: [
      {
        categories: data[0],
        labels: {
          rotation: -25
        }
      }
    ],

    yAxis: [
      {
        // first y-axis
        title: {
          text: "Failure Count Per IMSI"  // Goes on LHS of graph
        },
        min: 0
      }
    ],

	plotOptions: {
	  series: {
	  	borderWidth: 0,
	  	dataLabels: {
	  		enabled: true
	  	},
	    point: {
	      events: {
	        click: function(event) {
	          var chart = this.series.chart;
	          var imsi = this.imsi;
	          var name = this.name;
	          $.ajax({
	            url: rootURL + '/search/' + data[0][0],
	            success: function(data) {
	              swapSeries(chart,name,imsi,data);
	            },
	            dataType: "json"
	          });
	        }
	      }
	    }
	  }
	},


    series: [
      {
        name: "Failure Count Per IMSI",
        color: "#0071A7",
        type: "column",
        data: data[1],
        tooltip: {
          valueSuffix: ""
        }
      }
    ],

    tooltip: {
      shared: true
    },
    legend: {
      backgroundColor: "#ececec",
      shadow: true
    },
    credits: {
      enabled: false
    },
    noData: {
      style: {
        fontSize: "16px"
      }
    }
  });
}

 function swapSeries(chart, name, imsi,data) {
 	var dataJson = data; 
    chart.series[0].remove();
    chart.addSeries({
      data: data,
      name: name,
      imsi: imsi,
      colorByPoint: true
    });
  }

 
function setTableEvents(table) {
  // listen for page clicks
  table.on("page", () => {
    draw = true;
  });
 
  // listen for updates and adjust the chart accordingly
  table.on("draw", () => {
    if (draw) {
      draw = false;
    } else {
      const tableData = getTableData(table);
      createHighcharts(tableData);
    }
  });
}


//When the DOM is ready.
$(document).ready(function(){

	$(document).on("click", "#submitButton", function() {
	topTenIMSIFailures($('#fromDateTime').val(), $('#toDateTime').val());
	});

	$('#resetButton').on('click', function() {
    window.location.reload();
})

});