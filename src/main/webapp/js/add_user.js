function addNewUser() {
	document.getElementById('passwordNotEntered').innerHTML="";
	document.getElementById('usernameNotEntered').innerHTML="";
	document.getElementById('resultArea').innerHTML="";
	var url = 'rest/users';

	$('#resultArea').innerHTML = "";
	$('#passwordNotEntered').innerHTML = "";

	var formData = new FormData(document.querySelector('#createUser'));

	var username = formData.get("username");
	var password = formData.get("password");
	var role = formData.get("role");

	if(!username) {
		var resString = "<p>An email is required</p>";''
		$('#usernameNotEntered').append(resString);
		return
	}

	if(!password) {
		var resString = "<p>A password is required</p>";
		$('#passwordNotEntered').append(resString);
		return
	}

	if(!role) {
		var resString = "<p>A role is required</p>";
		$('#roleNotEntered').append(resString);
		return
	}


	$.ajax({
		url : url,
		type : 'POST',
		contentType : 'application/json',
		dataType : "json",
		data : JSON.stringify({
			"username" : username,
			"password" : password,
			"type" : role
		}),
		success : function(data) {
			var name = $("#username").val();
			var message = "User '" + name + "' has been added.";
			$("#resultArea").append(message);

			$("#username").val("");
			$("#password").val("");
		},
		error : function(data) {
			var name = $("#username").val();
			var message = "User '" + name + "' has been updated.";
			$("#resultArea").append(message);
		}
	});
}