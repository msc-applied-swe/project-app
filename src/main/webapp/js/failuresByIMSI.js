//var rootURL = "http://localhost:8080/project-app/rest/Event";
var rootURL = "rest/Event";
let startDate;
let endDate;
let imsiCode;

document.getElementById("fromDateTime").addEventListener("change", function() {
    var input = this.value;
	var month = String(new Date(input).getMonth()+1).padStart(2, '0');
	var year = String(new Date(input).getFullYear());
	var date = String(new Date(input).getDate()).padStart(2, '0');
	var hour = String(new Date(input).getHours()).padStart(2, '0');;
	var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
	startDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
    console.log(startDate);
});

document.getElementById("toDateTime").addEventListener("change", function() {
    var input = this.value;
	var month = String(new Date(input).getMonth()+1).padStart(2, '0');
	var year = String(new Date(input).getFullYear());
	var date = String(new Date(input).getDate()).padStart(2, '0');
	var hour = String(new Date(input).getHours()).padStart(2, '0');;
	var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
	endDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
    console.log(endDate);
});



var failuresByIMSI = function(startDate, endDate, imsiCode) {
	
	var st = startDate.replace('T', ' ')
	var et = endDate.replace('T', ' ')
	
	if (st == "Invalid Date or Time") {
		alert("The 'From' date/time field cannot be empty.");
		$("#fromDateTime").focus();
		return;
	}
	if (et == "Invalid Date or Time") {
		alert("The 'To' date field cannot be empty.");
		$("#toDateTime").focus();
		return;
	}
	
	document.getElementById("table_body").innerHTML = "";

	$.ajax({
		type: 'GET',
		url: rootURL + '/search/IMSI/' + st + '/' + et + '/' + imsiCode,
        contentType : "application/json",
        dataType : 'json',
        success : renderList
	});
}


var formatTotalDuration = function(numberOfSeconds) {
	var numberOfHours = Math.floor(numberOfSeconds/3600);
	remainingSeconds = numberOfSeconds - (numberOfHours*3600);
	var numberOfMins = Math.floor(remainingSeconds/60);
	remainingSeconds -= numberOfMins*60;
	var formattedTotalDuration = numberOfHours + " hr " + numberOfMins + " min " + remainingSeconds + " s";
	return formattedTotalDuration    
}


var renderList=function(data){
	var durationOfCall = 0;
	list=data;
	var str = "";
	str += "<h5>Total Call Failures For This IMSI: " + data.length + "<h5>";
	document.getElementById("resultSpace").innerHTML = str;
	
	$.each(list,function(index, projectdb){
		//durationOfCall += projectdb.duration;
		durationOfCall += projectdb[4];
		//$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+new Date(projectdb.date)+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td></tr>');
		$('#table_body').append('<tr><td>'+projectdb[0]+'</td><td>'+new Date(projectdb[1])+'</td><td>'+projectdb[2]+'</td><td>'+projectdb[3]+'</td><td>'+projectdb[4]+'</td></tr>');
		}); 
		var otherStr = "";
		otherStr += "<h5>Total duration for this IMSI: " + formatTotalDuration(durationOfCall) + "<h5>";
		document.getElementById("resultSpaceDuration").innerHTML = otherStr;
		
		$('#table_id').DataTable( {
  		"language": {
        search: "Search All Data:",
        searchPlaceholder: "Search..."
    }
} );

		const table = $("#table_id").DataTable();
			
					 // get table data
				  const tableData = getTableData(table);
				 
				  // create Highcharts
				  createHighcharts(tableData);
				  // table events
				  setTableEvents(table);   

};

//When the DOM is ready.
$(document).ready(function(){

	$(document).on("click", "#submitButton", function() {
	failuresByIMSI($('#fromDateTime').val(), $('#toDateTime').val(), $('#IMSI').val());
	});

	$('#resetButton').on('click', function() {
    window.location.reload();
})

});

function getTableData(table) {
  const dataArray = [],
  	IMSIArray = [],
  	DateArray = [],
  	causeCodeArray = [],
  	eventIdArray = [],
  	durationArray = []
 
  // loop table rows
  table.rows({ search: "applied" }).every(function() {
    const data = this.data();
    IMSIArray.push(data[0]);
    DateArray.push(data[1]);
    causeCodeArray.push(parseInt(data[2].replace(/\,/g, "")));
    eventIdArray.push(parseInt(data[3].replace(/\,/g, "")));
     durationArray.push(parseInt(data[4].replace(/\,/g, "")));
    
  });
 
  // store all data in dataArray
  dataArray.push(IMSIArray, DateArray, causeCodeArray, eventIdArray, durationArray);
 
  return dataArray;
}




function setTableEvents(table) {
  // listen for page clicks
  table.on("page", () => {
    draw = true;
  });
 
  // listen for updates and adjust the chart accordingly
  table.on("draw", () => {
    if (draw) {
      draw = false;
    } else {
      const tableData = getTableData(table);
      createHighcharts(tableData);
    }
  });
}


function createHighcharts(data) {
console.log(data[1]);
intArray  = data[4].map(Number);
  Highcharts.chart('failuresbyIMSIChart', {

    title: {
        text: 'Duration of Call failures'
    },

    subtitle: {
        text: 'Duration of call failures with respect to time for a given IMSI'
    },

    yAxis: {
        title: {
            text: 'Duration'
        }
    },

    xAxis: {
    title: {
            text: 'Date/Time'
        },
        categories: data[1],
        labels: {
          rotation: -25
        }
      },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
           
        }
    },

    series: [{
        name: 'Call Failures',
        data: intArray
    }],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

});
}



