
//$(document).ready(function() {
//	console.log("read");
//	sessionStorage.clear();
//	document.getElementById('passwordNotEntered').innerHTML="a";
//	$('#usernameNotEntered').innerHTML = "";
//	$('#passwordNotEntered').innerHTML = "";
//	$('#rejectionSpace').innerHTML = "";
//});



function verifyLogin() {
	sessionStorage.clear();
	document.getElementById('passwordNotEntered').innerHTML="";
	document.getElementById('usernameNotEntered').innerHTML="";
	document.getElementById('rejectionSpace').innerHTML="";
	var url = 'rest/users/login/'
	var formData = new FormData(document.querySelector('form'));
	var redirectTarget = "index.html";
	var username = formData.get("username");
	var password = formData.get("password");
	if(!username) {
		var resString = "<p>An email is required</p>";
		$('#usernameNotEntered').append(resString);
		return
	}
	
	if(!password) {
		var resString = "<p>A password is required</p>";
		$('#passwordNotEntered').append(resString);
		return
	}
	
	var responseCode;
	var responseUserType;
	
	$.ajax({
		url: url+username+"&"+password, 
		type: 'GET', 
		contentType: 'application/json',
		dataType: 'json',
		async: false,
		success: function(data, xhr) {
			if(data === undefined){
				var resString = "<p>Not found - please check credentials</p>";
				$('#rejectionSpace').append(resString);
				$('#usernameNotEntered').innerHTML = "";
				$('#passwordNotEntered').innerHTML = "";
			} else {
				responseUserType = data.type;
				var username = data.username;
				responseCode = xhr.status;
				var redirect = document.getElementById("loginRedirect");
				var type = responseUserType;
				sessionStorage.setItem("type",type);
				sessionStorage.setItem("username",username);
				redirect.href = "dashboard.html";
			}
		}
	})
}
 