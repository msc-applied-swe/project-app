
//var rootURL = "http://localhost:8080/project-app/rest/Event";
var rootURL = "rest/Event";
let startDate;
let endDate;
let phoneCode;

document.getElementById("fromDateTime").addEventListener("change", function() {
    var input = this.value;
	var month = String(new Date(input).getMonth()+1).padStart(2, '0');
	var year = String(new Date(input).getFullYear());
	var date = String(new Date(input).getDate()).padStart(2, '0');
	var hour = String(new Date(input).getHours()).padStart(2, '0');;
	var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
	startDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
});

document.getElementById("toDateTime").addEventListener("change", function() {
    var input = this.value;
	var month = String(new Date(input).getMonth()+1).padStart(2, '0');
	var year = String(new Date(input).getFullYear());
	var date = String(new Date(input).getDate()).padStart(2, '0');
	var hour = String(new Date(input).getHours()).padStart(2, '0');;
	var minute = String(new Date(input).getMinutes()).padStart(2, '0');;
	endDate = (year + '-' + month + '-' + date + ' ' + hour + ':' + minute);
});



var failuresByPhone = function(phoneCode, startDate, endDate) {
	
	var st = startDate.replace('T', ' ')
	var et = endDate.replace('T', ' ')
	
	if (st == "Invalid Date or Time") {
		alert("The 'From' date/time field cannot be empty.");
		$("#fromDateTime").focus();
		return;
	}
	if (et == "Invalid Date or Time") {
		alert("The 'To' date field cannot be empty.");
		$("#toDateTime").focus();
		return;
	}
	
	$.ajax({
		type: 'GET',
		url: rootURL + '/search/' + phoneCode + '/' + st + '/' + et,
		contentType : "application/json",
		dataType: "json",
		success: function(data) {
			renderList(data);
		}
	});
};



var renderList=function(data){
	list=data;
	var str = "";
	str += "<h5>Total Failures: " + data.length + "<h5>";
	document.getElementById("resultSpace").innerHTML = str;
	$.each(list,function(index, projectdb){
		//$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+new Date(projectdb.date)+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td></tr>');$('#table_body').append('<tr><td>'+projectdb.imsi+'</td><td>'+new Date(projectdb.date)+'</td><td>'+projectdb.causeCode+'</td><td>'+projectdb.eventId+'</td></tr>');
		$('#table_body').append('<tr><td>'+projectdb[0]+'</td><td>'+new Date(projectdb[1])+'</td><td>'+projectdb[2]+'</td><td>'+projectdb[3]+'</td></tr>');
		
		});  
		$('#table_id').DataTable( {
  		"language": {
        search: "Search All Data: ",
        searchPlaceholder: "Search..."
    }
} );

};

$(document).ready(function(){

	$(document).on("click", "#submitButton", function() {
	failuresByPhone($('#UEType').val(), $('#fromDateTime').val(), $('#toDateTime').val());
	});

	$('#resetButton').on('click', function() {
    window.location.reload();
})

});