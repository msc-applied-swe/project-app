package com.tus.mase.model;

import java.math.BigInteger;

public class UEEvent {

	private String model;
	private String causeCode;
	private int eventId;
	private String description;
	private BigInteger occurrences;

	public UEEvent() {
		super();
	}

	
	public UEEvent(final int eventId, final String causeCode, final String description, final String model, BigInteger occurrences) {
		super();
		this.model = model;
		this.causeCode = causeCode;
		this.eventId = eventId;
		this.description = description;
		this.occurrences = occurrences;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(String causeCode) {
		this.causeCode = causeCode;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public BigInteger getOccurrences() {
		return occurrences;
	}

	public void setOccurrences(BigInteger occurrences) {
		this.occurrences = occurrences;
	}

	
}
