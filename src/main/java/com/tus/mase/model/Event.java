package com.tus.mase.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "event")
public class Event {

	@Id // For JPA
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date")
	private Date date;
	private int eventId;
	private String failureClass;
	private int UEType;
	private int market;
	private int operator;
	private int cellId;
	private int duration;
	private String causeCode;
	private String neVersion;
	@Column(columnDefinition = "BIGINT")
	private BigInteger IMSI;
	@Column(columnDefinition = "BIGINT")
	private BigInteger HIER3_ID;
	@Column(columnDefinition = "BIGINT")
	private BigInteger HIER32_ID;
	@Column(columnDefinition = "BIGINT")
	private BigInteger HIER321_ID;

	public Event() {
	}

	public Event(final Date date, final int eventId, final String failureClass, final int uEType, final int market, final int operator, final int cellId,
			final int duration, final String causeCode, final String neVersion, final BigInteger iMSI, final BigInteger hIER3_ID,
			final BigInteger hIER32_ID, final BigInteger hIER321_ID) {
		super();
		this.date = date;
		this.eventId = eventId;
		this.failureClass = failureClass;
		UEType = uEType;
		this.market = market;
		this.operator = operator;
		this.cellId = cellId;
		this.duration = duration;
		this.causeCode = causeCode;
		this.neVersion = neVersion;
		IMSI = iMSI;
		HIER3_ID = hIER3_ID;
		HIER32_ID = hIER32_ID;
		HIER321_ID = hIER321_ID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String getFailureClass() {
		return failureClass;
	}

	public void setFailureClass(String failureClass) {
		this.failureClass = failureClass;
	}

	public int getUEType() {
		return UEType;
	}

	public void setUEType(int uEType) {
		UEType = uEType;
	}

	public int getMarket() {
		return market;
	}

	public void setMarket(int market) {
		this.market = market;
	}

	public int getOperator() {
		return operator;
	}

	public void setOperator(int operator) {
		this.operator = operator;
	}

	public int getCellId() {
		return cellId;
	}

	public void setCellId(int cellId) {
		this.cellId = cellId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getCauseCode() {
		return causeCode;
	}

	public void setCauseCode(String i) {
		this.causeCode = i;
	}

	public String getNeVersion() {
		return neVersion;
	}

	public void setNeVersion(String neVersion) {
		this.neVersion = neVersion;
	}

	public BigInteger getIMSI() {
		return IMSI;
	}

	public void setIMSI(BigInteger iMSI) {
		IMSI = iMSI;
	}

	public BigInteger getHIER3_ID() {
		return HIER3_ID;
	}

	public void setHIER3_ID(BigInteger hIER3_ID) {
		HIER3_ID = hIER3_ID;
	}

	public BigInteger getHIER32_ID() {
		return HIER32_ID;
	}

	public void setHIER32_ID(BigInteger hIER32_ID) {
		HIER32_ID = hIER32_ID;
	}

	public BigInteger getHIER321_ID() {
		return HIER321_ID;
	}

	public void setHIER321_ID(BigInteger hIER321_ID) {
		HIER321_ID = hIER321_ID;
	}

//	public int getId() {
//		return id;
//	}
//
//	public void setId(int id) {
//		this.id = id;
//	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", date=" + date + ", eventId=" + eventId + ", failureClass=" + failureClass
				+ ", UEType=" + UEType + ", market=" + market + ", operator=" + operator + ", cellId=" + cellId
				+ ", duration=" + duration + ", causeCode=" + causeCode + ", neVersion=" + neVersion + ", IMSI=" + IMSI
				+ ", HIER3_ID=" + HIER3_ID + ", HIER32_ID=" + HIER32_ID + ", HIER321_ID=" + HIER321_ID + "]";
	}

}
