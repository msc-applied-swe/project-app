
package com.tus.mase.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UEClass {

	@Id
	private long TAC;

	private String marketing;

	private String manufacturer;
	private String AccessCapability;
	private String model;
	private String vendorName;
	private String ueType;
	private String os;
	private String inputMode;
	
	
	
	public UEClass() {
		super();
	}



	public UEClass(long tac, String marketing, String manufacturer, String accessCapability, String model,
			String vendorName, String ueType, String os, String inputMode) {
		super();
		TAC = tac;
		this.marketing = marketing;
		this.manufacturer = manufacturer;
		AccessCapability = accessCapability;
		this.model = model;
		this.vendorName = vendorName;
		this.ueType = ueType;
		this.os = os;
		this.inputMode = inputMode;
	}



	public long getTAC() {
		return TAC;
	}



	public void setTAC(long tAC) {
		TAC = tAC;
	}



	public String getMarketing() {
		return marketing;
	}



	public void setMarketing(String marketing) {
		this.marketing = marketing;
	}



	public String getManufacturer() {
		return manufacturer;
	}



	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}



	public String getAccessCapability() {
		return AccessCapability;
	}



	public void setAccessCapability(String accessCapability) {
		AccessCapability = accessCapability;
	}



	public String getModel() {
		return model;
	}



	public void setModel(String model) {
		this.model = model;
	}



	public String getVendorName() {
		return vendorName;
	}



	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}



	public String getUeType() {
		return ueType;
	}



	public void setUeType(String ueType) {
		this.ueType = ueType;
	}



	public String getOs() {
		return os;
	}



	public void setOs(String os) {
		this.os = os;
	}



	public String getInputMode() {
		return inputMode;
	}



	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}
	

	

}
