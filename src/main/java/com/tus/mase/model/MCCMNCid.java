package com.tus.mase.model;

import java.io.Serializable;

public class MCCMNCid implements Serializable {
	private static final long serialVersionUID = -5215534506634462789L;
	private int mcc;
	private int mnc;

	public MCCMNCid() {
		super();
	}

	public MCCMNCid(int mcc, int mnc) {
		super();
		this.mcc = mcc;
		this.mnc = mnc;
	}
	
	public int getMcc() {
		return mcc;
	}

	public void setMcc(int mcc) {
		this.mcc = mcc;
	}

	public int getMnc() {
		return mnc;
	}

	public void setMnc(int mnc) {
		this.mnc = mnc;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mcc;
		result = prime * result + mnc;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
//		if (this == obj)
//			return true;
//		if (obj == null)
//			return false;
//		if (getClass() != obj.getClass())
//			return false;
		MCCMNCid other = (MCCMNCid) obj;
//		if (mcc != other.mcc)
//			return false;
//		if (mnc != other.mnc)
//			return false;
		return true;
	}

}
