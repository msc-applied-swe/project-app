package com.tus.mase.repository;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.time.LocalDateTime;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import com.tus.mase.model.Event;
import com.tus.mase.data.DatabaseHelper;
import com.tus.mase.data.EventCauseDAO;
import com.tus.mase.data.EventDAO;
import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.data.MCCMNCDAO;
import com.tus.mase.data.UEClassDAO;

@Path("/dataWS")
@Stateless
@LocalBean
public class DataReader {
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
	private static List<String> archive = new ArrayList<>();
	private static String filePath = "inputfolder/AIT Group Project 2022- Dataset 3A.xls";

	@EJB
	private DatabaseHelper databaseHelper;
	@EJB
	private EntityFactory entityFactory;
	@EJB
	private Validation validate;
	@EJB
	private EventDAO eventDAO;
	@EJB
	private EventCauseDAO eventCauseDAO;
	@EJB
	private FailureClassDAO failureClassDAO;
	@EJB
	private UEClassDAO ueClassDAO;
	@EJB
	private MCCMNCDAO mccMncDAO;

	@POST
	@Path("/LoadData")
	public void loadData() throws IOException {
		if (!archive.contains(filePath)) {
			archive.add(filePath);
			databaseHelper.truncateTables();
			InputStream inputStream = DataReader.class.getClassLoader().getResourceAsStream(filePath);
			HSSFWorkbook workBook = getWorkBook(inputStream);
			Arrays.asList(Sheets.values()).forEach(sheet -> readerExecute(workBook, sheet));
		} else {
			System.out.println("[INFO]::The Dataset [ " + filePath
					+ " ] has NOT been imported as a file of the same name has been loaded previously");
		}
	}

	public void readerExecute(HSSFWorkbook workBook, Sheets sheet) {

		HSSFSheet workBookSheet = workBook.getSheet(sheet.getSheetName());

		for (Row row : workBookSheet) {
			if (row.getRowNum() != 0) {
				HSSFCell[] cellData = new HSSFCell[row.getLastCellNum()];

				for (Cell cell : row) {
					cellData[cell.getColumnIndex()] = (HSSFCell) cell;
				}

				if (validate.validateData(cellData, workBookSheet)) {

					switch (workBookSheet.getSheetName()) {
					case "Base Data":
						eventDAO.save(getEntityFactory().createEvent(cellData));
						break;
					case "Event-Cause Table":
						eventCauseDAO.save(getEntityFactory().createEventCause(cellData));
						break;
					case "Failure Class Table":
						failureClassDAO.save(getEntityFactory().createFailureClass(cellData));
						break;
					case "UE Table":
						ueClassDAO.save(getEntityFactory().createUE(cellData));
						break;
					case "MCC - MNC Table":
						mccMncDAO.save(getEntityFactory().createMCCMNC(cellData));
						break;
					default:
						break;

					}
				} else {

					Event invalidEvent = getEntityFactory().createEvent(cellData);
					WriteToLog log = new WriteToLog();

					// Logging of invalid data
					LocalDateTime now = LocalDateTime.now();
					String file_seperator = System.getProperty("file.separator");
					String path = System.getProperty("user.home") + file_seperator + "Data" + file_seperator + "Logs"
							+ file_seperator;
					File file = new File(path + "invalid_data.txt");
					try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true))) {
						if (invalidEvent != null) {
							sendToLog(invalidEvent, log, now, bufferedWriter);
						}
					} catch (IOException e1) {
						// logger goes here
					}
				}
			}
		}
	}

	private void sendToLog(final Event invalidEvent, final WriteToLog log, final LocalDateTime now,
			final BufferedWriter bufferedWriter) {
		try {
			log.createLogEntry("[INFO]:: " + dtf.format(now) + " " + invalidEvent.toString()
					+ "\nVAILIDATION FAIL REASON:: " + validate.getDescription(), bufferedWriter);
		} catch (IOException e) {
			// logger goes here
		}
	}

	public HSSFWorkbook getWorkBook(InputStream inputStream) throws IOException {
		HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
		inputStream.close();
		return workBook;
	}

	// Testing
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath1) {
		DataReader.filePath = filePath1;
	}

	public void setValidate(Validation validate) {
		this.validate = validate;
	}

	public void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}

	public EntityFactory getEntityFactory() {
		return entityFactory;
	}

	public void setEventDAO(EventDAO eventDAO2) {
		this.eventDAO = eventDAO2;

	}

	public void setEventCauseDAO(EventCauseDAO eventCauseDAO2) {
		this.eventCauseDAO = eventCauseDAO2;
	}

	public void setFailureClassDAO(FailureClassDAO failureClassDAO2) {
		this.failureClassDAO = failureClassDAO2;
	}

	public void setUeClassDAO(UEClassDAO ueClassDAO) {
		this.ueClassDAO = ueClassDAO;
	}

	public void setMccMncDAO(MCCMNCDAO mccMncDAO) {
		this.mccMncDAO = mccMncDAO;
	}
}