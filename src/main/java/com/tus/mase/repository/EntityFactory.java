package com.tus.mase.repository;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.poi.hssf.usermodel.HSSFCell;

import com.tus.mase.model.Event;
import com.tus.mase.model.EventCause;
import com.tus.mase.model.FailureClass;
import com.tus.mase.model.MCCMNC;
import com.tus.mase.model.UEClass;

@Stateless
@LocalBean
public class EntityFactory {

	@PersistenceContext(unitName = "projectDB")
	private EntityManager em;

	public Event createEvent(HSSFCell[] cellData) {
		return new Event(cellData[0].getDateCellValue(), (int) cellData[1].getNumericCellValue(),
				cellData[2].toString(), (int) cellData[3].getNumericCellValue(),
				(int) cellData[4].getNumericCellValue(), (int) cellData[5].getNumericCellValue(),
				(int) cellData[6].getNumericCellValue(), (int) cellData[7].getNumericCellValue(),
				cellData[8].toString(), cellData[9].getStringCellValue(),
				new BigInteger(resturnCellAsString(cellData, 10)), new BigInteger(resturnCellAsString(cellData, 11)),
				new BigInteger(resturnCellAsString(cellData, 12)), new BigInteger(resturnCellAsString(cellData, 13)));
	}

	public EventCause createEventCause(HSSFCell[] cellData) {
		return new EventCause((int) cellData[0].getNumericCellValue(), (int) cellData[1].getNumericCellValue(),
				cellData[2].getStringCellValue());
	}

	public FailureClass createFailureClass(HSSFCell[] cellData) {
		return new FailureClass((int) cellData[0].getNumericCellValue(), cellData[1].getStringCellValue());
	}

	public UEClass createUE(HSSFCell[] cellData) {
		return new UEClass((long) cellData[0].getNumericCellValue(), cellData[1].toString(),
				cellData[2].getStringCellValue(), cellData[3].getStringCellValue(), cellData[4].toString(),
				cellData[5].getStringCellValue(), cellData[6].getStringCellValue(), cellData[7].getStringCellValue(),
				cellData[8].getStringCellValue());
	}

	public MCCMNC createMCCMNC(HSSFCell[] cellData) {
		return new MCCMNC((int) cellData[0].getNumericCellValue(), (int) cellData[1].getNumericCellValue(),
				cellData[2].getStringCellValue(), cellData[3].getStringCellValue());
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public EntityManager getEm() {
		return em;
	}
	
	public String resturnCellAsString(HSSFCell[] cellData, int cellNum) {
		double d = cellData[cellNum].getNumericCellValue();
		BigDecimal bd = BigDecimal.valueOf(d);
		// N.B. cell format - Excel displays only the first 15 significant digits!
		return bd.round(new MathContext(15)).toPlainString();
	}
}
