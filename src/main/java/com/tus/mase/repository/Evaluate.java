package com.tus.mase.repository;

public interface Evaluate<T> {
	boolean compareValues(T t);
}
