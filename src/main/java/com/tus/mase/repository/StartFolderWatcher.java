package com.tus.mase.repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.DependsOn;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
@DependsOn("FolderWatcher")
public class StartFolderWatcher {
	public enum States {
		BEFORESTARTED, STARTED, PAUSED, SHUTTINGDOWN
	};

	private States state;

	@EJB
	private FolderWatcher folderWatcher;

	@PostConstruct
	void init() throws InterruptedException {
		setState(States.BEFORESTARTED);
		setState(States.STARTED);
		System.out.println("Service Started");
		folderWatcher.startService();
	}

	@PreDestroy
	void destroy() {
		setState(States.SHUTTINGDOWN);
		System.out.println("Service Stopping...");
		folderWatcher.stopService();
		folderWatcher.stopFileWatcher();
	}

	public States getState() {
		return state;
	}

	public void setState(States state) {
		this.state = state;
	}

	public void setfolderWatcher(FolderWatcher fw) {
		this.folderWatcher = fw;
	}
}