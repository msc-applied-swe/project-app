package com.tus.mase.repository;

import javax.ejb.*;

import com.tus.mase.data.DatabaseHelper;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

@Singleton
@Lock(LockType.READ)
public class FolderWatcher {

	private WatchService watchService;
	private volatile boolean isCancelled = false;

	@EJB
	DataReader dataReader;
	@EJB
	DatabaseHelper databaseHelper;

	@Asynchronous
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void startService() throws InterruptedException {
		try {

			watchService = FileSystems.getDefault().newWatchService();

			Path dataDirectory = Paths
					.get(System.getProperty("user.home") + System.getProperty("file.separator") + "Data");

			// Registered events you interested in, created, modified and deleted
			WatchKey key = dataDirectory.register(watchService, StandardWatchEventKinds.ENTRY_CREATE);

			while (!isCancelled) {

				try {
					key = watchService.take();
				}catch (ClosedWatchServiceException e) {
					// Logger goes here
				} 

				List<WatchEvent<?>> keys = key.pollEvents();
				for (WatchEvent<?> watchEvent : keys) {
					WatchEvent.Kind<?> watchEventKind = watchEvent.kind();

					// This key is registered only
					// for ENTRY_CREATE, ENTRY_MODIFY events,
					// but an OVERFLOW event can
					// occur regardless if events
					// are lost or discarded.
					if (watchEventKind == StandardWatchEventKinds.OVERFLOW) {
						continue;
					}

					if (watchEventKind == StandardWatchEventKinds.ENTRY_CREATE) {
						// databaseHelper.truncateTables();
						dataReader.setFilePath("inputfolder/" + watchEvent.context().toString());
						dataReader.loadData();
					}
					key.reset();
				}
			}
		} catch (IOException e) {
			// Logger goes here
		}
	}

	public void stopService() {
		isCancelled = true;
	}

	public void stopFileWatcher() {
		try {
			if (watchService != null) {
				watchService.close();
			}

		} catch (IOException e) {
			// Logger here
		}
	}

	public WatchService getWatchService() {
		return watchService;
	}

	public void setWatchService(WatchService watchService) {
		this.watchService = watchService;
	}

	public boolean isCancelled() {
		return isCancelled;
	}
}
