package com.tus.mase.repository;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.DateUtil;

@Stateless
@LocalBean
public class Validation {
	private static String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		Validation.description = description;
	}

	public boolean validateData(HSSFCell[] cellData, HSSFSheet workBookSheet) {
		boolean dataIsValid = true;

		if (workBookSheet.getSheetName().equals(Sheets.BASE_DATA.getSheetName())) {

			// check for "(null)" in data
			for (HSSFCell cell : cellData) {
				if (cell.toString().equals("(null)")) {
					description = "Data is null";
					return false;
				}
			}

			// check cell data is in a valid date format
			if (!DateUtil.isCellDateFormatted(cellData[0])) {
				description = "Date is invalid";
				return false;
			}

			// less than greater than checks
			int eventId = (int) cellData[1].getNumericCellValue();
			if (lessThanCheck(eventId, 4000) || greaterThanCheck(eventId, 5000)) {
				description = "EventId is invalid";
				return false;
			}

			int failureClass = (int) cellData[2].getNumericCellValue();
			if (lessThanCheck(failureClass, 0) || greaterThanCheck(failureClass, 4)) {
				description = "Failure class is invalid";
				return false;
			}

			int ueType = (int) cellData[3].getNumericCellValue();
			if (lessThanCheck(ueType, 100000) || greaterThanCheck(ueType, 40000000)) {
				description = "UEType is invalid";
				return false;
			}

			int market = (int) cellData[4].getNumericCellValue();
			if (market == 345 || (lessThanCheck(market, 200) || greaterThanCheck(market, 900))) {
				description = "Market Value is invalid";
				return false;
			}

			int operator = (int) cellData[5].getNumericCellValue();
			if (operator == 935 || (lessThanCheck(operator, 1) || greaterThanCheck(operator, 1000))) {
				description = "Operator is invalid";
				return false;
			}

			int cellId = (int) cellData[6].getNumericCellValue();
			if (lessThanCheck(cellId, 1) || greaterThanCheck(cellId, 9999)) {
				description = "CellId is invalid";
				return false;
			}

			int duration = (int) cellData[7].getNumericCellValue();
			if (lessThanCheck(duration, 0)) {
				description = "Duration is invalid";
				return false;
			}

			int causeCode = (int) cellData[8].getNumericCellValue();
			if (lessThanCheck(causeCode, 0) || greaterThanCheck(causeCode, 30)) {
				description = "cause code is invalid";
				return false;
			}

			String neVersion = cellData[9].getStringCellValue();
			if (neVersion.length() != 3) {
				description = "NEVersion code is invalid";
				return false;
			}

		}
		return dataIsValid;
	}

	public boolean lessThanCheck(int num, int test) {
		Evaluate<Integer> lessThan = i -> i < test;
		return lessThan.compareValues(num);
	}

	public boolean greaterThanCheck(int num, int test) {
		Evaluate<Integer> greaterThan = i -> i > test;
		return greaterThan.compareValues(num);
	}
}
