package com.tus.mase.repository;

public enum Sheets {
	EVENT_CAUSE_TABLE("Event-Cause Table"), FAILURE_CLASS_TABLE("Failure Class Table"),
	UE_TABLE("UE Table"), MCC_MNC_TABLE("MCC - MNC Table"), BASE_DATA("Base Data");

	private String theSheetName;

	// Base Data, Event-Cause Table, Failure Class Table, UE Table, MCC - MNC Table
	Sheets(String aSheetName) {
		theSheetName = aSheetName;
	}

	public String getSheetName() {
		return theSheetName;
	}

}
