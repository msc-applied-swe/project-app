package com.tus.mase.repository;

import java.io.BufferedWriter;
import java.io.IOException;

public class WriteToLog {
	@SuppressWarnings("unused")
	private BufferedWriter bw;

	public void createLogEntry(String invalidData, BufferedWriter bw) throws IOException {
		try {
			bw.append(invalidData);
			bw.newLine();
		} finally {
			bw.close();
		}
	}

	// Testing
	public void setBw(BufferedWriter bw) {
		this.bw = bw;
	}
}
