package com.tus.mase.rest;

import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.FailureClass;


import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tus.mase.data.FailureClassDAO;
import com.tus.mase.model.FailureClass;

import java.util.List;

@Path("/FailureClass")
@Stateless
@Remote
public class FailureClassWS {

	@EJB
	private FailureClassDAO failureClassDAO;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllFailures() {
		List<FailureClass> failureList = failureClassDAO.getAllFailureClasses();
		return Response.status(200).entity(failureList).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/find/{id}")
	public Response getEventByIMSI(@PathParam("id") int id) {
		List<Event> resList = failureClassDAO.getFailureById(id);
		return Response.status(200).entity(resList).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllFailureClasses() {
		List<FailureClass> failureClassList = failureClassDAO.getAllFailureClasses();
		return Response.status(200).entity(failureClassList).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{failureClassID}")
	public Response getFailureClassById(@PathParam("failureClassID") int id) {
		FailureClass failureClass = failureClassDAO.getFailureClass(id);
		return Response.status(200).entity(failureClass).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveFailureClass(FailureClass failureClass) {
		failureClassDAO.save(failureClass);
		return Response.status(200).entity(failureClass).build();
	}

	public FailureClassDAO getFailureClassDAO() {
		return failureClassDAO;
	}

	public void setFailureClassDAO(FailureClassDAO failureClassDAO) {
		this.failureClassDAO = failureClassDAO;
	}
}
