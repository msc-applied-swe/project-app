package com.tus.mase.rest;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tus.mase.data.EventCauseDAO;
import com.tus.mase.model.EventCause;

@Path("/EventCause")
@Stateless
@Remote
public class EventCauseWS {

	@EJB
	private EventCauseDAO eventCauseDAO;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllEventCauses() {
		List<EventCause> eventCauseList = eventCauseDAO.getAllEventCauses();
		return Response.status(200).entity(eventCauseList).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{eventCauseID}")
	public Response getEventCausesById(@PathParam("eventCauseID") int id) {
		EventCause eventCause = eventCauseDAO.getEventCause(id);
		return Response.status(200).entity(eventCause).build();
	}


	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveEventCauses(EventCause eventCause) {
		eventCauseDAO.save(eventCause);
		return Response.status(200).entity(eventCause).build();
	}
	
	public EventCauseDAO getEventCauseDAO() {
		return eventCauseDAO;
	}

	public void setEventCauseDAO(EventCauseDAO eventCauseDAO) {
		this.eventCauseDAO = eventCauseDAO;
	}

}
