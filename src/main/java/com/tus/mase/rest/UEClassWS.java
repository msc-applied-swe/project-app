package com.tus.mase.rest;

import java.math.BigInteger;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tus.mase.data.EventDAO;
import com.tus.mase.data.UEEventIdCauseCodeDAO;
import com.tus.mase.model.Event;
import com.tus.mase.model.UEEvent;

@Path("/UEClass")
@Stateless
@Remote
public class UEClassWS {
	
	@EJB
	UEEventIdCauseCodeDAO uEEventIdCauseCodeDAO;
	
	List<Object[]> output;
	List<String> modelList;
	
	public UEEventIdCauseCodeDAO getuEEventIdCauseCodeDAO() {
		return uEEventIdCauseCodeDAO;
	}


	public void setuEEventIdCauseCodeDAO(UEEventIdCauseCodeDAO uEEventIdCauseCodeDAO) {
		this.uEEventIdCauseCodeDAO = uEEventIdCauseCodeDAO;
	}
	

	public List<Object[]> getOutput() {
		return output;
	}


	public void setOutput(List<Object[]> output) {
		this.output = output;
	}
	
	
	public List<String> getModelList() {
		return modelList;
	}


	public void setModelList(List<String> modelList) {
		this.modelList = modelList;
	}


	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("model/getAll/{model}")
	public Response getEventById(@PathParam("model") String model) {
		
		try {
			output = uEEventIdCauseCodeDAO.getEventIdCauseCodebyModelNumber(model);
			List <UEEvent> ueEventList = new ArrayList<>();
			output.forEach((obj) -> {
				ueEventList.add(new UEEvent((int)obj[0],(String)obj[1], (String)obj[2], model, (BigInteger)obj[3]));
			});
		
			return Response.status(200).entity(ueEventList).build();
		} catch (SQLException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}		
	}
	
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("model/getAll")
	public Response getAllModels() {
		
		try {
			modelList = uEEventIdCauseCodeDAO.getAllModels();
			return Response.status(200).entity(modelList).build();
			
		} catch (SQLException e) {
			return Response.serverError().entity(e.getMessage()).build();
		}		
	}
	
}
