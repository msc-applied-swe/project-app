package com.tus.mase.rest;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.tus.mase.data.EventDAO;
import com.tus.mase.model.Event;

@Path("/Event")
@Stateless
@Remote

public class EventWS {

	@EJB
	private EventDAO eventDAO;
	
	private final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm";

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllEvent() {
		List<Event> eventList = eventDAO.getAllEvents();
		return Response.status(200).entity(eventList).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("search/{IMSI}")
	public Response getEventByIMSI(@PathParam("IMSI") BigInteger search) {
		List<Event> foundIMSI = eventDAO.getEventByIMSI(search);
		return Response.status(200).entity(foundIMSI).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{ID}")
	public Response getEventById(@PathParam("ID") int id) {
		Event event = eventDAO.getEvent(id);
		return Response.status(200).entity(event).build();
	}
	
	
	//Removed try/catch and added throws declaration for testing, also removed seconds identifier in SimpleDateFormat 
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("search/{startDate}/{endDate}")
	public Response getEventFailureByDate(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate) throws ParseException {	
			SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
			Date sDate = formatter.parse(startDate);
			Date eDate = formatter.parse(endDate);
			List<Event> events = eventDAO.getEventByDate(sDate, eDate);
			return Response.status(200).entity(events).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("search/{phoneCode}/{startDate}/{endDate}")
	public Response getEventFailureByDateAndPhone
		(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate, @PathParam("phoneCode") int phoneCode) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);
		List<Event> events = eventDAO.getEventByDateAndPhone(sDate, eDate, phoneCode);
		return Response.status(200).entity(events).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("search/IMSI/{startDate}/{endDate}/{IMSI}")
	public Response getEventFailureByDateAndIMSI
		(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate, @PathParam("IMSI") BigInteger IMSI) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);
		List<Event> events = eventDAO.getEventByDateAndIMSI(sDate, eDate, IMSI);
		return Response.status(200).entity(events).build();
	}

	//GROUP-12
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("search/IMSI/{startDate}/{endDate}")
	public Response getTopTenIMSI
			(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);
		List<Event> events = eventDAO.getTopTenIMSI(sDate, eDate);
		return Response.status(200).entity(events).build();
	}

	//GROUP-11
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("combinations/{startDate}/{endDate}")
	public Response getTopTenCombinations
			(@PathParam("startDate") String startDate, @PathParam("endDate") String endDate) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(DATE_TIME_FORMAT);
		Date sDate = formatter.parse(startDate);
		Date eDate = formatter.parse(endDate);
		List<Event> events = eventDAO.getTopTenCombinations(sDate, eDate);
		return Response.status(200).entity(events).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveEvent(Event event) {
		eventDAO.save(event);
		return Response.status(200).entity(event).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("searchCauseCode/{IMSI}")
	public Response getCauseCodebyIMSI(@PathParam("IMSI") BigInteger IMSI) throws ParseException {

	List<String> causeCodebyIMSI = eventDAO.getcauseCodebyIMSI(IMSI);
	return Response.status(200).entity(causeCodebyIMSI).build();
}
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("getAllIMSI")
	public Response findAllIMSI() throws ParseException {

	List<String> ListOfIMSI = eventDAO.getAllIMSI();
	return Response.status(200).entity(ListOfIMSI).build();
}

	
	
	
	public EventDAO getEventDAO() {
		return eventDAO;
	}

	public void setEventDAO(EventDAO eventDAO) {
		this.eventDAO = eventDAO;
	}
	

}
