package com.tus.mase.rest;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


import com.tus.mase.data.UserDAO;
import com.tus.mase.model.Users;


@Path("/users")
@Stateless
//@Stateful
@LocalBean
public class UserWS {

	@EJB
	private UserDAO userDAO;
	private Response response;
	
	
	//Required for testing purposes only
	 public void setUserDao(UserDAO userDAO) {
		 this.userDAO = userDAO;
	 }
	 
	 public void setResponse(Response response) {
		 this.response = response;
	 }
	 
	 public UserDAO getUserDao() {
		 return userDAO;
	 }
	 
	 @GET
	 @Produces({MediaType.APPLICATION_JSON })
	 public Response FindAll() {
	     List<Users> users = userDAO.getAllUsers();
	     return Response.status(200).entity(users).build();
	 }
	 	 
	 @DELETE
	 @Path("/{id}")
	 @Produces({MediaType.APPLICATION_JSON })
	 public Response deleteUser(@PathParam("id") String name) {
	     userDAO.delete(name);
	     return Response.status(204).build();
	 }
	 
	 @PUT
	 @Path("/{id}")
	 @Produces({MediaType.APPLICATION_JSON })
	 @Consumes({"application/json"})
	 public Response updateUser(Users user) {
		 userDAO.update(user);
	     return Response.status(200).entity(user).build();
	 }
	 
	 
	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Users getUser(@PathParam("id") String username) {
		return userDAO.getUser(username);
	}

	// adds new user to the database
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String addUser(Users user) {
		//Users fetchedUser = userDAO.getUser(user.getUserId());
		Users fetchedUser = userDAO.getUser(user.getUsername());
		String message = "";
		userDAO.addUser(user);
		// checks if user doesn't already exist in database
		if (fetchedUser == null) {
			//userDAO.addUser(user);
			message = "{\"message\":\"User " + user.getUsername()
					+ " successfully added to the database\"}";
		} else {
			message = "{\"message\":\"User " + user.getUsername()
					+ " has been found\"}";
		}
		return message;
	}

	// logs-in user by checking for user name and password
	@GET
	@Path("/login/{id}&{password}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response userLogin(@PathParam("id") String name, @PathParam("password") String password) {
		response = Response.status(204).entity("fail").build();
		Users user = userDAO.getUser(name);
		if (isAuthenticated(user, password)) {
			response = Response.status(200).entity(user).build();
		} 
		return response;
	}

	private boolean isAuthenticated(Users user, String password) {
		return user != null && user.getPassword().equals(password);
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
