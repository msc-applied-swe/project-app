package com.tus.mase.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tus.mase.model.MCCMNC;
import com.tus.mase.model.MCCMNCid;

@Stateless
@LocalBean
public class MCCMNCDAO {
	
	@PersistenceContext(unitName = "projectDB")
	private EntityManager entityManager;

	public void save(MCCMNC mccmnc) {
		entityManager.persist(mccmnc);
	}

	@SuppressWarnings("unchecked")
	public List<MCCMNC> getAllMCCMNC() {
		Query query = entityManager.createQuery("SELECT m FROM MCCMNC m");
		return query.getResultList();
	}

	public MCCMNC getMCCMNC(final MCCMNCid mccmncId) {
		return entityManager.find(MCCMNC.class, mccmncId);
	}

	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
