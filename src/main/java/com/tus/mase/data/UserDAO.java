package com.tus.mase.data;

import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.tus.mase.model.Users;

@Stateless
@LocalBean
public class UserDAO {

	@PersistenceContext(unitName="userDB")
	private EntityManager entityManager;

	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<Users> getAllUsers() {
		Query query = entityManager.createQuery("SELECT u FROM Users u");
		return query.getResultList();
	}
	
	public Users getUser(String username) {
		return entityManager.find(Users.class, username);
	}
	
	public void update(Users user) {
		entityManager.merge(user);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void addUser(Users user) {
		Users tempUser = entityManager.find(Users.class, user.getUsername());
		
		if(tempUser == null) {
			entityManager.persist(user);
		} else {
			entityManager.merge(user);
		}
		
	}
	
	public void delete(String name) {
		entityManager.remove(getUser(name));
	}

}