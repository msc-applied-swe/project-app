package com.tus.mase.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tus.mase.model.UEClass;

@Stateless
@LocalBean
public class UEClassDAO {
	
	@PersistenceContext(unitName = "projectDB")
	private EntityManager entityManager;

	public void save(final UEClass ueClass) {
		entityManager.persist(ueClass);
	}
	
	@SuppressWarnings("unchecked")
	public List<UEClass> getAllUEClasses() {
		Query query = entityManager.createQuery("SELECT u FROM UEClass u");
		return query.getResultList();
	}

	public UEClass getUEClass(final long id) {
		return entityManager.find(UEClass.class, id);
	}
	
	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
