package com.tus.mase.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import com.tus.mase.model.EventCause;

@Stateless
@LocalBean
public class EventCauseDAO {

	@PersistenceContext(unitName="projectDB")
	private EntityManager entityManager;

	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public List<EventCause> getAllEventCauses() {
		Query query = entityManager.createQuery("SELECT u FROM EventCause u");
		return query.getResultList();
	}

	public EventCause getEventCause(final int id) {
		return entityManager.find(EventCause.class, id);
	}

	public void save(final EventCause eventCause) {
		entityManager.persist(eventCause);

	}
}
