package com.tus.mase.data;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.*;

import com.tus.mase.model.Event;
import com.tus.mase.model.EventCause;

@Stateless
@LocalBean
public class EventDAO {

	@PersistenceContext(unitName="projectDB")
	private EntityManager em;
	private Query query;
	
	public void setQuery(Query query) {
		this.query = query;
	}

	public List<Event> getAllEvents() {
		Query query = em.createQuery("SELECT u FROM Event u");
		return query.getResultList();
	}
	
	
	public List<Event> getEventByIMSI(BigInteger imsi) {
		Query query = em.createQuery("SELECT e from Event e WHERE e.IMSI=:IMSI");
		query.setParameter("IMSI", imsi);
		return query.getResultList();
	}
	
	
	
	
	public List<Event> getEventByDate(Date parsedDate1, Date parsedDate2) {
		//Narrowing search criteria for front end 
		//query = em.createQuery("SELECT u FROM Event u WHERE u.date BETWEEN :sDate AND :eDate");
		query = em.createQuery("SELECT u.IMSI, u.date, u.causeCode, u.eventId FROM Event u WHERE u.date BETWEEN :sDate AND :eDate");
		query.setParameter("sDate",parsedDate1);
		query.setParameter("eDate",parsedDate2);
		return query.getResultList();
	}
	
	
	//GROUP-8
	public List<Event> getEventByDateAndPhone(Date parsedDate1, Date parsedDate2, int phoneCode) {
		//Query query = em.createQuery("SELECT u FROM Event u WHERE UEType = :phoneCode AND u.date BETWEEN :sDate AND :eDate");
		Query query = em.createQuery("SELECT u.IMSI, u.date, u.causeCode, u.eventId FROM Event u WHERE UEType = :phoneCode AND u.date BETWEEN :sDate AND :eDate");
		query.setParameter("sDate",parsedDate1);
		query.setParameter("eDate",parsedDate2);
		query.setParameter("phoneCode",phoneCode);
		return query.getResultList();
	}
	
	//GROUP-9
	public List<Event> getEventByDateAndIMSI(Date parsedDate1, Date parsedDate2, BigInteger IMSI) {
		//Query query = em.createQuery("SELECT u FROM Event u WHERE IMSI = :IMSI AND u.date BETWEEN :sDate AND :eDate");
		Query query = em.createQuery("SELECT u.IMSI, u.date, u.causeCode, u.eventId, u.duration FROM Event u WHERE IMSI = :IMSI AND u.date BETWEEN :sDate AND :eDate");
		query.setParameter("sDate",parsedDate1);
		query.setParameter("eDate",parsedDate2);
		query.setParameter("IMSI",IMSI);
		return query.getResultList();
	}

	//GROUP-11
	public List<Event> getTopTenCombinations(Date parsedDate1, Date parsedDate2) {
		Query query1 = em.createQuery(
				"SELECT count(*),E.market,E.operator,E.cellId \n"
						+ "FROM Event E \n"
						+ "WHERE E.date BETWEEN :fromDate AND :toDate group by market,cellId,operator order by count(*) DESC \n");

		query1.setParameter("fromDate", parsedDate1);
		query1.setParameter("toDate", parsedDate2);
		query1.setMaxResults(10);
		return query1.getResultList();
	}

	//GROUP-12
	public List<Event> getTopTenIMSI(Date parsedDate1, Date parsedDate2) {
		//Unsure why this was changed, reverting so it works
		//Query query = em.createQuery("SELECT u.IMSI, count(u.IMSI), u.date, u.duration as numRes FROM Event u WHERE u.date BETWEEN :sDate AND :eDate group by u.IMSI order by count(u.IMSI) desc");
		Query query = em.createQuery("SELECT u.IMSI, count(u.IMSI) FROM Event u WHERE u.date BETWEEN :sDate AND :eDate group by u.IMSI order by count(u.IMSI) desc");
		query.setParameter("sDate",parsedDate1);
		query.setParameter("eDate",parsedDate2);
		query.setMaxResults(10);
		return query.getResultList();
	}


	public Event getEvent(final int id) {
		return em.find(Event.class, id);
	}

	public void save(final Event event) {
		em.persist(event);
	}

	public void update(final Event event) {
		em.merge(event);
	}

	public void delete(final int id) {
		em.remove(getEvent(id));
	}
//	For testing 

	public void setEntityManager(final EntityManager em) {
		this.em = em;
	}

	//Group-6
	public List<String> getcauseCodebyIMSI(BigInteger IMSI) {
		//Query query = em.createQuery("SELECT distinct causeCode from Event where IMSI=:IMSI");
		Query query = em.createQuery("select count(e.causeCode), e.causeCode from Event e where IMSI = :IMSI");
		query.setParameter("IMSI", IMSI);
		return query.getResultList();
		
		
	}

	//Group-6 for dynamic drop-down
	public List<String> getAllIMSI() {
		Query query = em.createQuery("select distinct IMSI from Event");
		return query.getResultList();
	}

}
