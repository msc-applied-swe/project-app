package com.tus.mase.data;

import java.util.List;
import com.tus.mase.model.Event;
import com.tus.mase.model.FailureClass;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
@LocalBean
public class FailureClassDAO {


	@PersistenceContext(unitName = "projectDB")
	private EntityManager entityManager;

	public List<FailureClass> getAllFailureClasses() {
		Query query = entityManager.createQuery("SELECT f FROM FailureClass f");
		return query.getResultList();
	}

	public List<Event> getFailureById(int id) {
		Query query = entityManager.createQuery("SELECT DISTINCT(e.IMSI), e.failureClass from Event e WHERE e.failureClass=:id");
		query.setParameter("id", String.valueOf(id)+".0");
		return query.getResultList();
	}
	public FailureClass getFailureClass(final int id) {
		return entityManager.find(FailureClass.class, id);
	}

	public void save(final FailureClass failureClass) {
		entityManager.persist(failureClass);

	}

	// Test methods
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(final EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
