package com.tus.mase.data;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class DatabaseHelper {

	@PersistenceContext(unitName = "projectDB")
	private EntityManager entityManager;

	private Query query;

	public void truncateTables() {
		query = entityManager.createNativeQuery("CALL truncate_tables()");
		query.executeUpdate();
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public void setQuery(Query query) {
		this.query = query;
	}
}
