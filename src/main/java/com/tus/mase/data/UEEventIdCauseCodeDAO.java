package com.tus.mase.data;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.tus.mase.model.Event;
import com.tus.mase.model.EventCause;
import com.tus.mase.model.UEEvent;

@Stateless
@LocalBean
public class UEEventIdCauseCodeDAO {


	@PersistenceContext(unitName="projectDB")
	private EntityManager entityManager;
	
	Query query;
	
	
	public EntityManager getEntityManager() {
		return entityManager;
	}


	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	


	public Query getQuery() {
		return query;
	}


	public void setQuery(Query query) {
		this.query = query;
	}


	public List<Object[]> getEventIdCauseCodebyModelNumber(String model) throws SQLException{
		String sqlQuery = "select a.eventId, a.causeCode, ec.description, a.occurrences from EventCause ec "
				+ "inner join ("
				//+ "select distinct eventId, causeCode,  count(*) as occurrences  from Event e where UEType in"
				+ "select distinct eventId, causeCode,  count(*) as occurrences  from event e where UEType in"
				+ " (SELECT TAC from UEClass WHERE model=:model) group by e.eventId, e.causeCode) as a "
				+ "on a.eventID = ec.eventId and a.causeCode = ec.causeCode";
		query = entityManager.createNativeQuery(sqlQuery);
		query.setParameter("model",model);
		return query.getResultList();
		
		
	} 
	
	public List<String> getAllModels() throws SQLException {
		//query = entityManager.createNativeQuery("SELECT distinct model from UEClass where TAC in (Select UEType from Event)");
		query = entityManager.createNativeQuery("SELECT distinct model from UEClass where TAC in (Select UEType from event)");
		
		return query.getResultList();
		
		
	}
	

}
